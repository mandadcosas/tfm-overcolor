# TFM - OverColor

Repositorio para la gestión del Trabajo de Fin de Máster.

## Importante

El proyecto ha de abrirse con el motor **Unity**. Ha sido desarrollado en la versión **2020.3.28f1**.

El proyecto tiene como objetivo el desarrollo de un videojuego para dispositivos móviles. Actualmente, esta siendo desarrollado para **Android**.

Para probar el proyecto es necesario conectar un dispositivo móvil Android a Unity mediante la aplicación oficial **Unity Remote 5**:
https://play.google.com/store/apps/details?id=com.unity3d.mobileremote&hl=es_419&gl=US

*Es posible que el proyecto también funcione en su estado actual para dispositivos Iphone cambiando el tipo de Build desde Unity.
Esto último no ha sido probado aún.*

Por otro lado, para **probar la versión ejecutable del juego**, ha de descargarse el archivo **"Ejecutable (APK).zip"**, descomprimirlo, e introducir el archivo **OverColorBuildFinal.apk** a una carpeta de un dispositivo Android. Tras esto, se podrá instalar el juego en el dispositivo móvil en cuestión.

Este README irá siendo actualizado a lo largo de la realización del proyecto.

## Controles

Los controles del juego son los siguientes:

- Para **mover** al personaje, debemos **tocar la pantalla** del móvil en la **zona izquierda**, y el personaje se desplazará a la línea más cercana al dedo.
- Para **atacar**, debemos pulsar los **botones de colores**, realizando un ataque del color del botón. Tienen un tiempo de enfriamiento.

## Vídeo

El vídeo demostrativo de la versión más reciente puede hallarse en el siguiente enlace:

https://youtu.be/rWld9rx_gEk 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que gestiona la pantalla de victoria
public class VictoryManager : MonoBehaviour
{
    public MeshRenderer Background;         //MeshRenderer del fondo

    public float TimeInEnd = 0;             //Tiempo transcurrido en la escena de fin
    public float Adjuster = 1;              //Parametro para ajustar la velocidad del fondo

    // Start is called before the first frame update
    void Start()
    {
        PlayerStats.Victories++;
        SaveSystem.SaveGameData();
    }

	private void Update()
	{
        //El temporizador siempre va subiendo
        TimeInEnd += Time.deltaTime;

        Vector2 offset = new Vector2(0, -1 * TimeInEnd / Adjuster);
        Background.material.mainTextureOffset = offset;
    }
}

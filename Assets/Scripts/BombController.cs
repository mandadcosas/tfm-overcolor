using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase que controla el funcionamiento de la bomba de color 
public class BombController : MonoBehaviour
{
    public GameplayManager GM;			//GameplayManager de la escena

    public bool InCooldown;         //Booleano que indica si el boton esta en cooldown o no

    public float TimePassed;        //Tiempo que ha pasado desde que el boton entro en enfriamiento

    public ParticleSystem RedParticles; //Sistema de particulas rojas
    public ParticleSystem BlueParticles; //Sistema de particulas azules
    public ParticleSystem YellowParticles; //Sistema de particulas amarillas

    public AudioSource Explosion; //Audio con el sonido de explosion

    // Llamada al inicio
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        InCooldown = false;
        TimePassed = 0f;
    }

    // Llamada en cada frame
    void Update()
    {
        //Si no esta en cooldown, avanzamos el contador
        if (InCooldown)
        {
            TimePassed += Time.deltaTime; //Aumentamos el contador de tiempo de enfriamiento

            //En caso de que se supere el tiempo de enfriamiento establecido
            if (TimePassed > GM.ButtonCD * 10)
            {
                InCooldown = false; //Se termina el enfriamiento
                TimePassed = 0f; //Reiniciamos el contador
                gameObject.GetComponent<Button>().interactable = true; //Se podra volver a interactuar con el boton
            }
        }
    }

    //Funcion llamada cuando se pulsa el boton de la bomba
    public void BombButtonPressed()
	{
        InCooldown = true; //El boton pasara a estar en enfriamiento
        gameObject.GetComponent<Button>().interactable = false; //Se dejara de poder interactuar con el boton mientras dure el enfriamiento

        //Hacemos un ataque de pulso de todos los colores
        GM.PulseAttack(E_COLORS.RED);
        GM.PulseAttack(E_COLORS.BLUE);
        GM.PulseAttack(E_COLORS.YELLOW);

        //Lo repetimos por si hay enemigos dobles
        GM.PulseAttack(E_COLORS.RED);
        GM.PulseAttack(E_COLORS.BLUE);
        GM.PulseAttack(E_COLORS.YELLOW);

        RedParticles.Play();
        BlueParticles.Play();
        YellowParticles.Play();

        Explosion.Play();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para destruir objetos cuya utilidad es reproducir un sonido y morir
public class AudioDestroyer : MonoBehaviour
{
    public AudioSource Sound;


    // Update is called once per frame
    void Update()
    {
        if (!Sound.isPlaying) Destroy(gameObject); //Si se termina el audio, rompemos el objeto
        
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

//Clase que gestiona la funcionalidad de los botones de la pantalla de fin de partida
public class EndButtonsController : MonoBehaviour
{
	//Funcion para volver a la escena de la villa
    public void ReturnToVillage()
	{
		SceneManager.LoadScene("Village");
	}

	//Funcion para volver a la escena del gameplay
	public void Replay()
	{
		SceneManager.LoadScene("Game");
	}
}
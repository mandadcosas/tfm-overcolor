using UnityEngine;
using UnityEngine.UI;

//Clase que controla el funcionamiento y enfriamiento de los botones de colores
//Cada boton tiene su propia instancia de la clase
public class ColorButtonController : MonoBehaviour
{
    public GameplayManager GM;      //GameplayManager de la escena
    public bool InCooldown;         //Booleano que indica si el boton esta en cooldown o no

    public float TimePassed;        //Tiempo que ha pasado desde que el boton entro en enfriamiento

    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena

        //Los botones empiezan listos para ser usados
        InCooldown = false; 
        TimePassed = 0f; 
    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        //Comprobamos que el boton este en enfriamiento
		if (InCooldown)
		{
            TimePassed += Time.deltaTime; //Aumentamos el contador de tiempo de enfriamiento

            //En caso de que se supere el tiempo de enfriamiento establecido
            if(TimePassed > GM.ButtonCD)
			{
                InCooldown = false; //Se termina el enfriamiento
                TimePassed = 0f; //Reiniciamos el contador
                gameObject.GetComponent<Button>().interactable = true; //Se podra volver a interactuar con el boton
            }
		}
    }

    //Funcion llamada cuando se presiona el boton rojo
    public void RedButtonPressed()
	{
        InCooldown = true; //El boton pasara a estar en enfriamiento
        gameObject.GetComponent<Button>().interactable = false; //Se dejara de poder interactuar con el boton mientras dure el enfriamiento

        GM.AttackOnLine(GM.Roloc.CurrentLine, E_COLORS.RED); //Realizaremos un ataque de color ROJO en la linea en la que se encuentre el jugador
        GM.Roloc.GetComponent<Animator>().SetTrigger("Attack");

        if(GM.MirrorActive) GM.AttackOnLine(GM.MirrorLine, E_COLORS.RED);
    }

    //Funcion llamada cuando se presiona el boton azul
    public void BlueButtonPressed()
    {
        InCooldown = true; //El boton pasara a estar en enfriamiento
        gameObject.GetComponent<Button>().interactable = false; //Se dejara de poder interactuar con el boton mientras dure el enfriamiento

        GM.AttackOnLine(GM.Roloc.CurrentLine, E_COLORS.BLUE); //Realizaremos un ataque de color AZUL en la linea en la que se encuentre el jugador 
        GM.Roloc.GetComponent<Animator>().SetTrigger("Attack");

        if (GM.MirrorActive) GM.AttackOnLine(GM.MirrorLine, E_COLORS.BLUE);
    }

    //Funcion llamada cuando se presiona el boton amarillo
    public void YellowButtonPressed()
    {
        InCooldown = true; //El boton pasara a estar en enfriamiento
        gameObject.GetComponent<Button>().interactable = false; //Se dejara de poder interactuar con el boton mientras dure el enfriamiento

        GM.AttackOnLine(GM.Roloc.CurrentLine, E_COLORS.YELLOW); //Realizaremos un ataque de color AMARILLO en la linea en la que se encuentre el jugador 
        GM.Roloc.GetComponent<Animator>().SetTrigger("Attack");

        if (GM.MirrorActive) GM.AttackOnLine(GM.MirrorLine, E_COLORS.YELLOW);
    }
}
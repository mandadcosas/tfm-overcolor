using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Enumerado con los colores de enemigos, divisas, etc
public enum E_COLORS { RED, BLUE, YELLOW, NONE };

//Clase que gestiona el flujo del gameplay/juego
public class GameplayManager : MonoBehaviour
{   
    public GameObject Line1;                        //GameObject con la primera linea del escenario
    public GameObject Line2;                        //GameObject con la segunda linea del escenario
    public GameObject Line3;                        //GameObject con la tercera linea del escenario

    public RolocController Roloc;                   //Controlador del avatar del jugador
    public GameObject RedRay;                       //Prefab del disparo de color rojo
    public GameObject BlueRay;                      //Prefab del disparo de color azul
    public GameObject YellowRay;                    //Prefab del disparo de color amarillo

    public GameObject WhiteRay;                     //Particulas blancas que aparecen tras explosion de bomba enemiga
    public GameObject HurtCanvas;                    //Canvas con el panel a mostrar cuando se pierde vida
    public Image HurtPanelImage;                    //Ppanel a mostrar cuando se pierde vida

    public List<EnemyController> EnemiesLine1;      //Lista con los enemigos presentes en la Linea 1
    public List<EnemyController> EnemiesLine2;      //Lista con los enemigos presentes en la Linea 2
    public List<EnemyController> EnemiesLine3;      //Lista con los enemigos presentes en la Linea 3

    public float TimeTotal;                         //Float con el tiempo total de la partida

    //ROLOC STATS
    public int MaxHP;                               //Entero con los puntos de vida maximos del jugador
    public int currentHP;                           //Entero con los puntos de vida actuales del jugador

    public float BaseButtonCD;                      //Float con el tiempo de enfriamiento base de los botones
    public float ButtonCD;                          //Float con el porcentaje de reduccion de enfriamiento de los botones
    public float CDReduction;                       //Float con el tiempo de enfriamiento actual/en uso de los botones

    public int ExtraLives;                          //Entero con el numero de vidas extra del jugador restantes
    public float RareUpgradeChance;                 //Float con la probabilidad de obtener una Mejora del tipo Rara

    public int HeartRate = 10;                      //Ratio de aparicion de corazones (a menor su valor, mas corazones aparecen)
    public int RedMultiplier = 1;                   //Multiplicador de la divisa roja obtenida
    public int BlueMultiplier = 1;                  //Multiplicador de la divisa roja obtenida
    public int YellowMultiplier = 1;                //Multiplicador de la divisa amarilla obtenida

    public List<GameObject> UpgradeList;            //Lista con las Mejoras obtenidas por el jugador durante la run

    //HUD
    public Text TimeText;                           //Texto con el tiempo de juego
    public Text HealthText;                         //Texto con los puntos de vida del jugador
    public Text LivesText;

    public Text RedCurrencyText;                    //Texto con la divisa roja que posee el jugador
    public Text BlueCurrencyText;                   //Texto con la divisa azul que posee el jugador
    public Text YellowCurrencyText;                 //Texto con la divisa amarilla que posee el jugador

    public GameObject BombButton;                   //Boton especial para usar la bomba de color
    public GameObject Turret;                       //GameObject de la Torreta que disparara rayos
    public GameObject Mirror;                       //GameObject del clon de Roloc

    public bool MirrorActive = false;               //Booleano que indica si el clon de Roloc esta activo o no
    public int MirrorLine = 1;                      //Linea en la que debe permanecer el clon de Roloc

    public float EnemyRateMultiplier = 1;           //Multiplicador del ratio al que aparecen los enemigos de forma normal

    public Color ColorHurt;                         //Color a mostrar por pantalla cuando se pierde vida
    public Color ColorHeal;                         //Color a mostrar por pantalla cuando se recibe una curacion

    public AudioSource HurtSound;                   //Sonido de perder vida
    public AudioSource HealSound;                   //Sonido de recibir curacion

    public bool Inmunity = false;                   //Booleano que indica si Roloc ha de perder vida o no al ser golpeada
    

    //Funcion llamada automaticamente al instanciarse el script
    public void Awake()
	{
        //Inicializamos los parametros relacionados con el enfriamiento
        //Lo hacemos en Awake() para asegurarnos de que los controladores de los botones dispongan lo antes posible de la informacion
        BaseButtonCD = 3f;
        CDReduction = PlayerStats.CDReduction;
        ButtonCD = BaseButtonCD - (BaseButtonCD * CDReduction / 100);
    }

    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        TimeTotal = 0f; //Inicializamos el contador del tiempo

        //Inicializamos los diversos stats del jugador
        MaxHP = PlayerStats.MaxHP;
        currentHP = MaxHP;
        ExtraLives = PlayerStats.ExtraLives;
        RareUpgradeChance = PlayerStats.RareChance;
        UpgradeList = new List<GameObject>();

        LivesText.text = ExtraLives.ToString();

        //Actualizamos el texto de la vida para mostrar la vida actual
        HealthText.text = currentHP + " / " + MaxHP;

        SpawnInitialItems();
    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        //Actualizamos el tiempo de juego y lo mostramos por el HUD
        TimeTotal += Time.deltaTime;
        int timeInt = (int) TimeTotal;
        TimeText.text = timeInt + " s";

        //Actualizamos los valores de las divisas en el HUD
        RedCurrencyText.text = Currencies.RedCurrency.ToString();
        BlueCurrencyText.text = Currencies.BlueCurrency.ToString();
        YellowCurrencyText.text = Currencies.YellowCurrency.ToString();

        //Actualizamos el valor de los puntos de vida en el HUD
        HealthText.text = currentHP + " / " + MaxHP;
    }

    //Funcion por la que se realiza un ataque de un color especifico en una linea especifica
    //El parametro line representa la linea del ataque, del 1 al 3
    //El parametro color representa el color del ataque
    public void AttackOnLine(int line, E_COLORS color)
	{
        GameObject usedLine = null; //Objeto en el que almacenaremos la linea en la que realizar el ataque

        //En caso de que sea la primera linea
        if (line == 1)
		{
            usedLine = Line1; //Indicaremos que la primera linea recibira el ataque

            //Iteraremos por todos los enemigos de la linea
            for (int i = EnemiesLine1.Count - 1; i >= 0; i--)
			{
                //Los enemigos con valor nulo seran eliminados de la lista
                if (EnemiesLine1[i] == null)
                {
                    EnemiesLine1.RemoveAt(i);
                }

                bool killed = EnemiesLine1[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
                if (killed) EnemiesLine1.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
            }
        }
        //En caso de que sea la segunda linea
        else if ( line == 2)
		{
            usedLine = Line2; //Indicaremos que la segunda linea recibira el ataque

            //Iteraremos por todos los enemigos de la linea
            for (int i = EnemiesLine2.Count - 1; i >= 0; i--)
            {
                //Los enemigos con valor nulo seran eliminados de la lista
                if (EnemiesLine2[i] == null)
				{
                    EnemiesLine2.RemoveAt(i);
                }

                bool killed = EnemiesLine2[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
                if (killed) EnemiesLine2.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
            }
        }
        //En caso de que sea la tercera linea
        else if (line == 3)
        {
            usedLine = Line3; //Indicaremos que la tercera linea recibira el ataque

            //Iteraremos por todos los enemigos de la linea
            for (int i = EnemiesLine3.Count - 1; i >= 0; i--)
            {
                //Los enemigos con valor nulo seran eliminados de la lista
                if (EnemiesLine3[i] == null)
                {
                    EnemiesLine3.RemoveAt(i);
                }

                bool killed = EnemiesLine3[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
                if (killed) EnemiesLine3.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
            }
        }

        GameObject usedColor = null; //GameObject en el que almacenaremos el prefab del rayo a mostrar como parte del disparo

        //Si el color es rojo, usaremos el rayo rojo
        if (color == E_COLORS.RED)
		{
            usedColor = RedRay;
		}
        //Si el color es azul, usaremos el rayo azul
        else if (color == E_COLORS.BLUE)
		{
            usedColor = BlueRay;
        }
        //Si el color es amarillo, usaremos el rayo amarillo
        else if (color == E_COLORS.YELLOW)
        {
            usedColor = YellowRay;
        }

        //Instanciamos el rayo en la linea en la que se realizo el ataque
        Instantiate(usedColor, usedLine.transform.position, Quaternion.identity);
    }

    //Funcion por la que se realiza un ataque de pulso de un color especifico en todas las lineas
    public void PulseAttack(E_COLORS color)
	{
        //Iteraremos por todas las lineas e iremos destruyendo a los enemigos del color del pulso

        //Iteraremos por todos los enemigos de la primera linea
        for (int i = EnemiesLine1.Count - 1; i >= 0; i--)
        {
            //Los enemigos con valor nulo seran eliminados de la lista
            if (EnemiesLine1[i] == null)
            {
                EnemiesLine1.RemoveAt(i);
            }

            bool killed = EnemiesLine1[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
            if (killed) EnemiesLine1.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
        }

        //Iteraremos por todos los enemigos de la segunda linea
        for (int i = EnemiesLine2.Count - 1; i >= 0; i--)
        {
            //Los enemigos con valor nulo seran eliminados de la lista
            if (EnemiesLine2[i] == null)
            {
                EnemiesLine2.RemoveAt(i);
            }

            bool killed = EnemiesLine2[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
            if (killed) EnemiesLine2.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
        }

        //Iteraremos por todos los enemigos de la tercera linea
        for (int i = EnemiesLine3.Count - 1; i >= 0; i--)
        {
            //Los enemigos con valor nulo seran eliminados de la lista
            if (EnemiesLine3[i] == null)
            {
                EnemiesLine3.RemoveAt(i);
            }

            bool killed = EnemiesLine3[i].KillByPlayer(color); //Realizaremos el ataque sobre el enemigo
            if (killed) EnemiesLine3.RemoveAt(i); //Si el enemigo fue derrotado, lo borraremos de la lista
        }
    }

    //Funcion por la que se le aplicaran penalizaciones al jugador por sel golpeado
    public void DamageRoloc()
	{
        if (Inmunity) return;

        currentHP -= 1; //Se reducen los puntos de vida del jugador
        HealthText.text = currentHP + " / " + MaxHP; //Se actualiza el HUD con la vida del jugador

        HurtPanelImage.color = ColorHurt;
        HurtCanvas.GetComponent<Animator>().SetTrigger("Hurt");
        HurtSound.Play();

        //En caso de que la vida baje a 0 se termina la partida
        if(currentHP <= 0)
		{
            if(ExtraLives > 0)
			{
                currentHP = MaxHP;
                ExtraLives--;

                LivesText.text = ExtraLives.ToString();
                return;
			}

            EndValues.Time = TimeTotal; //Almacenamos el tiempo total de la partida
            SceneManager.LoadScene("End"); //Cargamos la escena de fin de partida
        }
    }

    public void HealRoloc()
	{
        HurtPanelImage.color = ColorHeal;
        HurtCanvas.GetComponent<Animator>().SetTrigger("Hurt");
        HealSound.Play();

        currentHP += 1; //Se reducen los puntos de vida del jugador
        if (currentHP > MaxHP) currentHP = MaxHP;
        HealthText.text = currentHP + " / " + MaxHP; //Se actualiza el HUD con la vida del jugador

    }

    //Funcion por la que se actualiza el enfriamiento y la reduccion de enfriamiento de los botones
    //Recibe un parametro CDR correspondiente al aumento en la reduccion de enfriamiento deseada
    public void UpdateCDR(float CDR)
	{
        CDReduction += CDR; //Aumentamos el porcentaje de reduccion de enfriamiento
        ButtonCD = BaseButtonCD - (BaseButtonCD * CDReduction / 100); //Recalculamos el enfriamiento
    }

    //Funcion que genera una explosion realizada por el enemigo que quita vida al jugador
    public void ExplosionFromEnemy(int line)
	{
        //En base a la linea, determinamos la posicion
        GameObject usedLine = null;
        if (line == 1)
        {
            usedLine = Line1;

        }
        else if (line == 2)
        {
            usedLine = Line2;
        }
        else
        {
            usedLine = Line3;
        }
        Instantiate(WhiteRay, usedLine.transform.position, Quaternion.identity); //Instanciamos un rayo blanco

        //Si el jugador esta en la linea de la explision, pierde vida
        if (line == Roloc.CurrentLine)
        {
            DamageRoloc();        
        }
    }

    //Funcion por la que inicializamos la torreta del jugador
    public void ActivateTurret(E_COLORS primary, E_COLORS secondary)
	{
        if (!Turret.activeSelf) Turret.SetActive(true); //Si la torreta no esta activa, la activamos

        TurretController tc = Turret.GetComponent<TurretController>();

        //En base al color de entrada, activamos en la torreta los ataques de ese color
        if(primary == E_COLORS.RED && secondary == E_COLORS.NONE)
		{
            tc.RedTurret = true;
		}

        if (primary == E_COLORS.BLUE && secondary == E_COLORS.NONE)
        {
            tc.BlueTurret = true;
        }

        if (primary == E_COLORS.YELLOW && secondary == E_COLORS.NONE)
        {
            tc.YellowTurret = true;
        }

        //Tambien tenemos en cuenta los colores dobles
        if ((primary == E_COLORS.RED && secondary == E_COLORS.YELLOW) || (primary == E_COLORS.YELLOW && secondary == E_COLORS.RED))
        {
            tc.OrangeTurret = true;
        }

        if ((primary == E_COLORS.RED && secondary == E_COLORS.BLUE) || (primary == E_COLORS.BLUE && secondary == E_COLORS.RED))
        {
            tc.PurpleTurret = true;
        }

        if ((primary == E_COLORS.YELLOW && secondary == E_COLORS.BLUE) || (primary == E_COLORS.BLUE && secondary == E_COLORS.YELLOW))
        {
            tc.GreenTurret = true;
        }

    }

    //Funcion por la que se le otorgan objetos extra al jugador al comienzo de la partida
    public void SpawnInitialItems()
	{
        if (PlayerStats.StartingItems <= 0) return; //Si el jugador no deberia tener objetos iniciales, no se hace nada

        List<GameObject> basicOptions = GameObject.Find("UpgradeGenerator").GetComponent<UpgradeGenerator>().BasicUpgrades;

        //Instanciaremos tantos objetos como sean necesarios
        //No podran ser raros
        for(int i = 0; i < PlayerStats.StartingItems; i++)
		{
            int r = Random.Range(0, basicOptions.Count);

            GameObject newItem = Instantiate(basicOptions[r]);
            basicOptions.RemoveAt(r);

            UpgradeList.Add(newItem);

        }
    }
}
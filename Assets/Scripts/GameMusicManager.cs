using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Clase que controla la transicion entre canciones
public class GameMusicManager : MonoBehaviour
{
    public AudioSource[] Audios;    //Array con los diferentes audios posibles

    public int CurrentSong = 0;     //Indice de la cancion que suena actualmente

    public float SongTimer = 0;     //Tiempo transcurrido desde que comenzo la ultima cancion


    // Update is called once per frame
    void Update()
    {
        SongTimer += Time.deltaTime;

        //Si la cancion no debe loopear y hemos llegado al fin de la misma, cambiamos a la siguiente
        if(!Audios[CurrentSong].loop && SongTimer >= Audios[CurrentSong].clip.length)
		{
            SongTimer = 0;
            NextSong();
		}
    }

    //Funcion que cambia la cancion actual a la siguiente
    public void NextSong()
	{
        Audios[CurrentSong].Stop();

        CurrentSong++;
        if (CurrentSong >= Audios.Length) CurrentSong = 0;

        Audios[CurrentSong].Play();
	}

    //Funcion que pausa la cancion actual
    public void PauseSong()
	{
        Audios[CurrentSong].Pause();
	}

    //Funcion que reanuda la cancion actual
    public void ContinueSong()
	{
        Audios[CurrentSong].UnPause();
	}
}

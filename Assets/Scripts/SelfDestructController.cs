using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que gestiona la autodestruccion de un GameObject al cabo de un retardo predefinido
public class SelfDestructController : MonoBehaviour
{
    public float TimeToLive = 5f;       //Tiempo a pasar hasta que el objeto se destruya
    private float timeLived = 0f;       //Tiempo transcurrido desde la creacion del objeto


    // Update is called once per frame
    void Update()
    {
        timeLived += Time.deltaTime;

        if(timeLived >= TimeToLive)
		{
            Destroy(gameObject);
		}
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorController : MonoBehaviour
{
    public GameplayManager GM;          //GameplayManager de la escena

    public float Cooldown = 15f;        //CD del espejo
    public float Duration = 5f;         //Duracion del espejo

    public bool ActiveMirror = false;   //Bool que indica si esta activo el espejo

    public float TimeSinceLast = 0f;    //Tiempo desde que se activo el espejo

    public int Line = 1;                //Linea del espejo

    public SpriteRenderer Sprite;       //SpriteRenderer del espejo

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena
        Sprite = GetComponent<SpriteRenderer>();
        Sprite.enabled = false;

        //En base a la linea, colocamos al espejo basandonos en las lineas ya puestas en la escena
        Line = GM.Roloc.CurrentLine;

        if (Line == 1)
        {
            transform.position = new Vector3(transform.position.x, GM.Line1.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
        }
        if (Line == 2)
        {
            transform.position = new Vector3(transform.position.x, GM.Line2.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
        }

        if (Line == 3)
        {
            transform.position = new Vector3(transform.position.x, GM.Line3.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        TimeSinceLast += Time.deltaTime;

        //Desactivacion del espejo
        if (ActiveMirror && TimeSinceLast > Duration)
		{
            TimeSinceLast = 0f;
            ActiveMirror = false;
            GM.MirrorActive = false;
            Sprite.enabled = false;
        }

        //Activacion del espejo
        if (!ActiveMirror && TimeSinceLast > Cooldown)
        {
            TimeSinceLast = 0f;
            ActiveMirror = true;
            GM.MirrorActive = true;
         
            Line = GM.Roloc.CurrentLine;
            GM.MirrorLine = Line;

            Sprite.enabled = true;

            if (Line == 1)
            {
                transform.position = new Vector3(transform.position.x, GM.Line1.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
            }
            if (Line == 2)
            {
                transform.position = new Vector3(transform.position.x, GM.Line2.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
            }

            if (Line == 3)
            {
                transform.position = new Vector3(transform.position.x, GM.Line3.transform.position.y - GM.Roloc.OffsetY, transform.position.z);
            }

        }

    }
}

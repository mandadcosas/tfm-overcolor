using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase con los parametros que conforman a una zona/nivel del juego
public class Zone : MonoBehaviour
{
    public Color ZoneColor;             //Color del fondo de la zona
    public int ZoneNumber = 0;          //Numero de la zona

    public int SpawnRate = 0;           //Ratio de aparicion de enemigos
    public float Speed = 1.5f;          //Velocidad de los enemigos
    public float Duration = 0;          //Duracion de la zona

    public int DoubleColorChance = 0;   //Probabilidad de que los enemigos sean bicolor
    public int DarkColorChance = 0;     //Probabilidad de que los enemigos sean de color oscuro
    public int ShieldedChance = 0;      //Probabilidad de que los enemigos tengan escudo
    public int ExplosiveChance = 0;     //Probabilidad de que los enemigos sean explosivos
    public int ShooterChance = 0;       //Probabilidad de que los enemigos sean disparadores
    public int GroundedChance = 0;      //Probabilidad de que los enemigos sean intangibles/subterraneos
    public int JumperChance = 0;        //Probabilidad de que los enemigos sean saltarines

}

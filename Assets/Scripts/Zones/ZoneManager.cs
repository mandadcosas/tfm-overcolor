using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Clase que gestiona la transicion y cambio entre zonas/niveles
public class ZoneManager : MonoBehaviour
{
    public float TimeInCurrentZone = 0f;        //Tiempo transcurrido en la zona actual

    public List<Zone> Zones;                    //Lista con todas las zonas del juego
    public int CurrentZoneIndex = 0;            //Indice de la zona que se esta atravesando actualmente

    public Zone CurrentZone;                    //Zona que se esta atravesando actualmente

    public EnemyGenerator EnemyGen;             //Generador de enemigos de la escena
    //public Camera MainCam;                      //Camara principal de la escena
    public UpgradeGenerator UpgradeGen;         //Generador de mejoras de la escena

    public bool ZoneEnding = false;             //Boleano que indica si la zona ya esta acabando o no
    public float ZoneEndingDuration = 3f;       //Duracion del final de zona

    public bool ZoneStarting = true;            //Booleano que indica si la zona esta aun empezado
    public float ZoneStartingDuration = 3f;     //Duracion del comienzo de zona

    public bool ItemPickedHere = false;         //Booleano que indica si el jugador ya recibio una mejora de esta zona o no
    public float DelaySinceItemPicked = 2f;     //Tiempo a transcurrir desde la obtencion de la mejora y el final de la zona

    public GameObject ZoneCanvas;               //Canvas que indica al jugador en que zona se encuentra
    public Text ZoneText;                       //Texto que indica al jugador en que zona se encuentra

    public MeshRenderer Background;             //MeshRenderer del suelo en movimiento
    public SpriteRenderer Filter;               //Filtro para cambiar el color del suelo
    public float TotalTimer = 0;                //Tiempo pasado desde que se instancio el objeto
    public float Adjuster = 1;                  //Parametro para ajustar la velocidad a la que se mueve el fondo

    public GameplayManager GM;                  //GameplayManager de la escena
    public SpriteRenderer UpBar;                //Barra superior de separacion de lineas
    public SpriteRenderer MiddleBar;            //Barra de en medio de separacion de lineas
    public SpriteRenderer DownBar;              //Barra inferior de separacion de lineas 

    public GameObject Buttons;                  //GameObject con el canvas con los botones
    public GameObject WinnerRoloc;              //GameObjecto con una copia de roloc con la animacion de victoria
    
    
    // Start is called before the first frame update
    void Start()
    {
        //Obtenemos la zona inicial
        CurrentZone = Zones[CurrentZoneIndex];
        EnemyGen.CurrentZone = CurrentZone;
        //MainCam.backgroundColor = CurrentZone.ZoneColor;
        Filter.color = CurrentZone.ZoneColor;
        UpBar.color = CurrentZone.ZoneColor;
        MiddleBar.color = CurrentZone.ZoneColor;
        DownBar.color = CurrentZone.ZoneColor;

        //Preparacion del inicio de zona con texto
        ZoneCanvas.SetActive(true);
        ZoneText.text = "Zona " + CurrentZone.ZoneNumber;
        EnemyGen.StopGeneration = true;
        ZoneStarting = true;
    }

    // Update is called once per frame
    void Update()
    {
        //El temporizador siempre va subiendo
        TimeInCurrentZone += Time.deltaTime;
        TotalTimer += Time.deltaTime;

        Vector2 offset = new Vector2(GM.EnemyRateMultiplier * TotalTimer * CurrentZone.Speed / Adjuster, 0);
        Background.material.mainTextureOffset = offset;

        //Fin del inicio de ronda
        if (ZoneStarting && TimeInCurrentZone >= ZoneStartingDuration)
        {
            TimeInCurrentZone = 0; //Reseteamos el temporizador

            //Desactivamos el mensaje de inicio de zona
            ZoneCanvas.SetActive(false);
            EnemyGen.StopGeneration = false; //Como ha comenzado la ronda, queremos que se generen enemigos
            ZoneStarting = false;
        }

        //Fin del final de ronda y comienzo de la siguiente ronda
        if (ZoneEnding && TimeInCurrentZone >= ZoneEndingDuration)
		{
            TimeInCurrentZone = 0; //Reseteamos el temporizador

            ZoneEnding = false;
            
            //Cargamos la siguiente zona
            CurrentZoneIndex++;

            //Si no quedan mas zonas, el jugador ha ganado
            if (CurrentZoneIndex >= Zones.Count)
            {
                SceneManager.LoadScene("Victory");
                return;
            }
         
            CurrentZone = Zones[CurrentZoneIndex];
            EnemyGen.CurrentZone = CurrentZone;
            //MainCam.backgroundColor = CurrentZone.ZoneColor;
            Filter.color = CurrentZone.ZoneColor;
            UpBar.color = CurrentZone.ZoneColor;
            MiddleBar.color = CurrentZone.ZoneColor;
            DownBar.color = CurrentZone.ZoneColor;

            //Preparacion del inicio de zona con texto
            ZoneCanvas.SetActive(true);
            ZoneText.text = "Zona " + CurrentZone.ZoneNumber;           
            ZoneStarting = true;

            ItemPickedHere = false;
            
        }

        //En caso de que la zona este acabando y aun no se haya recogido la mejora, se le ofrece al jugador
        if(ZoneEnding && !ItemPickedHere && TimeInCurrentZone >= ZoneEndingDuration - DelaySinceItemPicked)
		{           
            UpgradeGen.GenerateUpgrades();
            ItemPickedHere = true;
        }

        //Si la zona esta comenzando o terminando, no se hace nada mas
        if (ZoneStarting || ZoneEnding) return;
		

        //Si se supera la duracion de la zona, la zona comienza a terminar
        if(TimeInCurrentZone >= CurrentZone.Duration)
		{
            TimeInCurrentZone = 0;
            
            //Preparacion del final de ronda
            ZoneEnding = true;
            EnemyGen.StopGeneration = true; //No queremos que se generen enemigos al final de ronda

            //Destruimos a todos los enemigos que aun existan

            GM.PulseAttack(E_COLORS.BLUE);
            GM.PulseAttack(E_COLORS.RED);
            GM.PulseAttack(E_COLORS.YELLOW);

            GM.PulseAttack(E_COLORS.BLUE);
            GM.PulseAttack(E_COLORS.RED);
            GM.PulseAttack(E_COLORS.YELLOW);

            GM.PulseAttack(E_COLORS.BLUE);
            GM.PulseAttack(E_COLORS.RED);
            GM.PulseAttack(E_COLORS.YELLOW);

            GM.PulseAttack(E_COLORS.BLUE);
            GM.PulseAttack(E_COLORS.RED);
            GM.PulseAttack(E_COLORS.YELLOW);

            //Si es la ronda final, activamos la animacion final
            if (CurrentZoneIndex == Zones.Count - 1)
			{
                GM.Inmunity = true;
                GM.Roloc.gameObject.SetActive(false);
                Buttons.SetActive(false);
                WinnerRoloc.SetActive(true);

                ItemPickedHere = true;             
            }
        }

    }
}

using System;
using UnityEngine;

//Clase que gestiona el movimiento del avatar del jugador por la pantalla
public class RolocController : MonoBehaviour
{
    public Transform Line1;     //Transform de la Linea 1 del escenario
    public Transform Line2;     //Transform de la Linea 2 del escenario
    public Transform Line3;     //Transform de la Linea 3 del escenario

    public int CurrentLine;     //Entero que indica la linea en la que el jugador se encuentra actualmente

    private float oldY;         //Float que almacena la posicion Y original en la que se toco la pantalla (Actualmente en desuso)

    public float OffsetY = 0;   //Diferencia del sprite de Roloc con respecto a la linea

    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        transform.position = new Vector3(transform.position.x, Line2.transform.position.y - OffsetY, transform.position.z); //Colocamos inicialmente al jugador en la Linea 2
        CurrentLine = 2; //Indicamos que el jugador se encuentra en la Linea 2
        oldY  = 0f; //Originalmente, diremos que la pantalla se toco en un punto central.
    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        //Iteramos por todos los toques en la pantalla
        for (int i = 0; i < Input.touchCount; i++)
		{
            Touch thisTouch = Input.touches[i];

            //En caso de que el toque en cuestion se encuentre en la zona izquierda de la pantalla
            if (Camera.main.ScreenToWorldPoint(thisTouch.position).x < 0)
            {
                float actualY = Camera.main.ScreenToWorldPoint(thisTouch.position).y; //Almacenamos la posicion Y del toque

                //Calculamos la distancia entre el toque y cada una de las lineas
                float proxToLine1 = Math.Abs(Line1.position.y - actualY);
                float proxToLine2 = Math.Abs(Line2.position.y - actualY);
                float proxToLine3 = Math.Abs(Line3.position.y - actualY);

                //En caso de que la linea mas cercana sea la Linea 1
                if(proxToLine1 < proxToLine2 && proxToLine1 < proxToLine3)
				{
                    CurrentLine = 1; //Indicaremos que el jugador se encuentra en la Linea 1
                    transform.position = new Vector3(transform.position.x, Line1.transform.position.y - OffsetY, transform.position.z); //Moveremos el avatar a la Linea 1

                    GetComponent<SpriteRenderer>().sortingOrder = -3;
                }
                //En caso de que la linea mas cercana sea la Linea 2
                else if (proxToLine2 < proxToLine1 && proxToLine2 < proxToLine3)
                {
                    CurrentLine = 2; //Indicaremos que el jugador se encuentra en la Linea 2
                    transform.position = new Vector3(transform.position.x, Line2.transform.position.y - OffsetY, transform.position.z); //Moveremos el avatar a la Linea 2

                    GetComponent<SpriteRenderer>().sortingOrder = 3;
                }
                //En caso de que la linea mas cercana sea la Linea 3
                else if (proxToLine3 < proxToLine1 && proxToLine3 < proxToLine2)
                {
                    CurrentLine = 3; //Indicaremos que el jugador se encuentra en la Linea 3
                    transform.position = new Vector3(transform.position.x, Line3.transform.position.y - OffsetY, transform.position.z); //Moveremos el avatar a la Linea 3

                    GetComponent<SpriteRenderer>().sortingOrder = 7;
                }
            }
        }        
    }

    //Funcion de movimiento por la que se cambia de linea mediante un drag hacia arriba o hacia abajo
    //Actualmente esta en desuso
    void OldMovement(Touch thisTouch)
	{
        //En caso de que el toque en cuestion se encuentre en la zona izquierda de la pantalla
        if (Camera.main.ScreenToWorldPoint(thisTouch.position).x < 0)
        {
            //Si el toque acaba de comenzar, almacenamos su posicion Y
            if (thisTouch.phase == TouchPhase.Began)
            {
                oldY = Camera.main.ScreenToWorldPoint(thisTouch.position).y;
            }

            //Si el toque acaba de terminar
            if (thisTouch.phase == TouchPhase.Ended)
            {
                //Almacenamos la posicion Y en la que se termino el toque
                float actualY = Camera.main.ScreenToWorldPoint(thisTouch.position).y;

                //Si el toque se solto notoriamente mas arriba de donde se empezo a pulsar
                if (actualY > oldY + 1f)
                {
                    MoveRolocUp(); //Subimos al jugador a la linea por encima de la actual
                }
                //Si el toque se solto notoriamente mas abajo de donde se empezo a pulsar
                else if (actualY < oldY - 1f)
                {
                    MoveRolocDown(); //Bajamos al jugador a la linea por debajo de la actual
                }
            }
        }
    }

    //Funcion por la que subimos al jugador una linea hacia arriba
    void MoveRolocUp()
	{
        //Cambiaremos la linea y posicion del jugador en 1 linea hacia arriba
        //Si ya estaba en la Linea 1, no pasara nada
        if(CurrentLine == 1)
		{
            return;
		}
        else if(CurrentLine == 2)
		{
            CurrentLine = 1;
            transform.position = new Vector3(transform.position.x, Line1.transform.position.y, transform.position.z);
        }
        else if(CurrentLine == 3)
		{
            CurrentLine = 2;
            transform.position = new Vector3(transform.position.x, Line2.transform.position.y, transform.position.z);
        }
	}

    //Funcion por la que subimos al jugador una linea hacia abaj
    void MoveRolocDown()
	{
        //Cambiaremos la linea y posicion del jugador en 1 linea hacia abajo
        //Si ya estaba en la Linea 3, no pasara nada
        if (CurrentLine == 1)
        {
            CurrentLine = 2;
            transform.position = new Vector3(transform.position.x, Line2.transform.position.y, transform.position.z);
        }
        else if (CurrentLine == 2)
        {
            CurrentLine = 3;
            transform.position = new Vector3(transform.position.x, Line3.transform.position.y, transform.position.z);
        }
        else if (CurrentLine == 3)
        {
            return;
        }
    }
}
//Clase estatica que almacena los niveles de los edificios
public static class BuildingLevels
{
	public static int TallerLevel = 0;		//Nivel del Taller
	public static int CasaLevel = 0;		//Nivel de la Casa
	public static int CarreterasLevel = 0;  //Nivel de las Carreteras
	public static int BosqueLevel = 0;		//Nivel del Bosque
	public static int MercadoLevel = 0;		//Nivel del Mercado
	public static int AeropuertoLevel = 0;	//Nivel del Aeropuerto
}
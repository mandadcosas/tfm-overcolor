//Clase estatica que almacena las divisas que posee el jugador
public static class Currencies
{
    public static int RedCurrency = 0;      //Cantidad de divisa roja
    public static int BlueCurrency = 0;     //Cantidad de divisa azul
    public static int YellowCurrency = 0;   //Cantidad de divisa amarilla
}
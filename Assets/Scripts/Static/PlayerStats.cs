//Clase estatica que almacena las estadisticas del jugador
public static class PlayerStats
{
    public static int MaxHP = 10;           //Puntos de vida maximos del jugador
    public static float CDReduction = 0;    //Reduccion de enfriamiento de los botones
    public static float RareChance = 5;    //Probabilidad de obtener objetos raros
    public static int StartingItems = 0;    //Cantidad de objetos con los que se inicia una run
    public static int ExtraLives = 0;       //Cantidad de vidas extra
    public static int UnlockedBuildings = 0;    //Numero de edificios ya desbloqueados

    public static int Victories = 0;        //Numero de victorias en el juego
}
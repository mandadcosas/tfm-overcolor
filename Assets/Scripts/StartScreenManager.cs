using UnityEngine;
using UnityEngine.SceneManagement;

//Clase con la funcionalidad de la pantalla de inicio
public class StartScreenManager : MonoBehaviour
{
	private void Start()
	{
		SaveSystem.LoadGameData();
	}

	//Funcion llamada en cada frame de la ejecucion
	void Update()
    {
        //En caso de que se toque la pantalla y se levante el dedo, se cargara la escena de la villa
        if (Input.touchCount > 0)
		{
            if(Input.touches[0].phase == TouchPhase.Ended)
			{
                SceneManager.LoadScene("Village");
            }        
        }
    }   
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el comportamiento de los corazones dropeables
public class HeartController : MonoBehaviour
{
    public int Line = 1;                    //Linea del corazon

    public GameplayManager GM;              //GameplayManager
    public bool Hitted;                     //Booleano que indica si ya ha golpeado al jugador o no
    public float BaseSpeed = 3f;            //Velocidad base a la que se mueve el corazon


    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena
        Hitted = false; //Originalmente el corazon no habran golpeado al jugador
    }

    // Update is called once per frame
    void Update()
    {
        //El corazon avanzara por su linea en direccion al jugador
        //La velocidad de los corazones ira escalando ligeramente con el tiempo de partida
        transform.Translate(Vector3.left * Time.deltaTime * BaseSpeed * (1 + GM.TimeTotal / 150));

        //En caso de que el corazon alcance la posicion del jugador y no le haya golpeado aun
        if (transform.position.x < GM.Roloc.transform.position.x + 0.2f && !Hitted && GM.Roloc.CurrentLine == Line)
        {
            Hitted = true; //Indicaremos que el corazon ya ha golpeado al jugador
            GM.HealRoloc(); //Se realizara la curacion al jugador
            Destroy(gameObject);
        }

        if (transform.position.x < (GM.Roloc.transform.position.x - 3) && !Hitted)
        {
            Hitted = true; //Indicaremos que el corazon ya ha golpeado al jugador     
            Destroy(gameObject);
        }

    }
}

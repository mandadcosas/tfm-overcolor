using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//Clase que gestiona el guardado y carga de datos
public static class SaveSystem
{
	//Funcion por la que guardamos en un fichero el progreso del juego
	public static void SaveGameData()
	{
		BinaryFormatter formatter = new BinaryFormatter();
		string path = Application.persistentDataPath + "/overcolor.data";

		FileStream stream = new FileStream(path, FileMode.Create);

		GameData data = new GameData();

		formatter.Serialize(stream, data);
		stream.Close();
	}

	//Funcion por la que cargamos desde un fichero los datos del juego
	public static void LoadGameData()
	{
		string path = Application.persistentDataPath + "/overcolor.data";

		if (File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

			GameData data = formatter.Deserialize(stream) as GameData;
			stream.Close();

			//Actualizamos nuestros estaticos con la nueva info
			PlayerStats.MaxHP = data.MaxHP;
			PlayerStats.CDReduction = data.CDReduction;
			PlayerStats.RareChance = data.RareChance;
			PlayerStats.StartingItems = data.StartingItems;
			PlayerStats.ExtraLives = data.ExtraLives;
			PlayerStats.UnlockedBuildings = data.UnlockedBuildings;
			PlayerStats.Victories = data.Victories;

			BuildingLevels.TallerLevel = data.TallerLevel;
			BuildingLevels.CasaLevel = data.CasaLevel;
			BuildingLevels.CarreterasLevel = data.CasaLevel;
			BuildingLevels.BosqueLevel = data.BosqueLevel;
			BuildingLevels.MercadoLevel = data.MercadoLevel;
			BuildingLevels.AeropuertoLevel = data.AeropuertoLevel;

			Currencies.RedCurrency = data.RedCurrency;
			Currencies.BlueCurrency = data.BlueCurrency;
			Currencies.YellowCurrency = data.YellowCurrency;

		}
		
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase serializable en la que se almacenarian todos los datos del juego que nos interesa guardar entre partidas
[Serializable]
public class GameData
{
    //Datos de PlayerStats
    public  int MaxHP;           
    public  float CDReduction;    
    public  float RareChance;
    public  int StartingItems;
    public  int ExtraLives;
    public  int UnlockedBuildings;
    public  int Victories;

    //Datos de BuildingLevels
    public  int TallerLevel;      
    public  int CasaLevel;        
    public  int CarreterasLevel;  
    public  int BosqueLevel;
    public  int MercadoLevel;
    public  int AeropuertoLevel;

    //Datos de Currencies
    public  int RedCurrency;      
    public  int BlueCurrency;     
    public  int YellowCurrency;   


    //Constructor por el que guardamos los datos en las variables desde los distintos estaticos
    public GameData()
	{
        MaxHP = PlayerStats.MaxHP;
        CDReduction = PlayerStats.CDReduction;
        RareChance = PlayerStats.RareChance;
        StartingItems = PlayerStats.StartingItems;
        ExtraLives = PlayerStats.ExtraLives;
        UnlockedBuildings = PlayerStats.UnlockedBuildings;
        Victories = PlayerStats.Victories;

        TallerLevel = BuildingLevels.TallerLevel;
        CasaLevel = BuildingLevels.CasaLevel;
        CarreterasLevel = BuildingLevels.CarreterasLevel;
        BosqueLevel = BuildingLevels.BosqueLevel;
        MercadoLevel = BuildingLevels.MercadoLevel;
        AeropuertoLevel = BuildingLevels.AeropuertoLevel;

        RedCurrency = Currencies.RedCurrency;
        BlueCurrency = Currencies.BlueCurrency;
        YellowCurrency = Currencies.YellowCurrency;
	}
}

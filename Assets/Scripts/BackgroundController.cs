using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el movimiento de los fondos
public class BackgroundController : MonoBehaviour
{
    public float Timer = 0;             //Tiempo que lleva activo el fondo
    public float Speed = 1.5f;          //Velocidad a la que mover
    public float Adjuster = 10f;        //Parametro para ajustar la velocidad final dividiendola
    public MeshRenderer Background;     //MeshRenderer del fondo


    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;

        //Movemos la textura del fondo
        Vector2 offset = new Vector2(Timer * Speed / Adjuster,0);
        Background.material.mainTextureOffset = offset;
    }

}

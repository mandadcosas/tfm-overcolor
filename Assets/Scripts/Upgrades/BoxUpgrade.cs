using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "La Caja";
        this.Description = "Obten un objeto aleatorio.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 27; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "La Caja";
        this.Description = "Obten un objeto aleatorio.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena

        List<GameObject> basicOptions = GameObject.Find("UpgradeGenerator").GetComponent<UpgradeGenerator>().BasicUpgrades;
        List<GameObject> rareOptions = GameObject.Find("UpgradeGenerator").GetComponent<UpgradeGenerator>().RareUpgrades;

        int total = basicOptions.Count + rareOptions.Count;

        int r = Random.Range(0, total);

        GameObject newItem = null;

        if(r < basicOptions.Count)
		{
            newItem = Instantiate(basicOptions[r]);
            basicOptions.RemoveAt(r);

        }
		else
		{
            newItem = Instantiate(rareOptions[r - basicOptions.Count]);
            rareOptions.RemoveAt(r - basicOptions.Count);
        }

        GM.UpgradeList.Add(newItem);
        //options.Remove(newItem);
    }
}

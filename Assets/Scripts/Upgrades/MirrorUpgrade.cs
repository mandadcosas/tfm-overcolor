using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Espejo";
        this.Description = "Crea un clon de Roloc de vez en cuando que la imita.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 25; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Espejo";
        this.Description = "Crea un clon de Roloc de vez en cuando que la imita.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena

        GM.Mirror.SetActive(true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowTurretUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Torreta Amarilla";
        this.Description = "Dispara frecuentemente un rayo amarillo.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 21; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Torreta Amarilla";
        this.Description = "Dispara frecuentemente un rayo amarillo.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.ActivateTurret(E_COLORS.YELLOW, E_COLORS.NONE);

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    public GameplayManager GM;

    public int Line = 1;
    public GameObject LineObject;

    public float TimeBetweenShots = 7f;
    public float TimeSinceRed = 3.5f;
    public float TimeSinceBlue = 3.5f;
    public float TimeSinceYellow = 3.5f;
    public float TimeSinceOrange = 3.5f;
    public float TimeSincePurple = 3.5f;
    public float TimeSinceGreen = 3.5f;


    public bool RedTurret = false;
    public bool BlueTurret = false;
    public bool YellowTurret = false;
    public bool OrangeTurret = false;
    public bool PurpleTurret = false;
    public bool GreenTurret = false;

    public GameObject OrangeRay;
    public GameObject PurpleRay;
    public GameObject GreenRay;

    


    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena

        Line = Random.Range(1, 4);

        if (Line == 1) {
            transform.position = new Vector3(transform.position.x, GM.Line1.transform.position.y, transform.position.z);
            LineObject = GM.Line1;

            GetComponent<SpriteRenderer>().sortingOrder = -5;
        }
        if (Line == 2) {
            transform.position = new Vector3(transform.position.x, GM.Line2.transform.position.y, transform.position.z);
            LineObject = GM.Line2;

            GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
        
        if (Line == 3) {
            transform.position = new Vector3(transform.position.x, GM.Line3.transform.position.y, transform.position.z);
            LineObject = GM.Line3;

            GetComponent<SpriteRenderer>().sortingOrder = 5;
        }

    }

    // Update is called once per frame
    void Update()
    {

		if (RedTurret)
		{
            TimeSinceRed += Time.deltaTime;

            if(TimeSinceRed > TimeBetweenShots)
			{
                TimeSinceRed = 0;
                ShootRed();
			}
		}

        if (BlueTurret)
        {
            TimeSinceBlue += Time.deltaTime;

            if (TimeSinceBlue > TimeBetweenShots)
            {
                TimeSinceBlue = 0;
                ShootBlue();
            }
        }

        if (YellowTurret)
        {
            TimeSinceYellow += Time.deltaTime;

            if (TimeSinceYellow > TimeBetweenShots)
            {
                TimeSinceYellow = 0;
                ShootYellow();
            }
        }

        if (OrangeTurret)
        {
            TimeSinceOrange += Time.deltaTime;

            if (TimeSinceOrange > TimeBetweenShots)
            {
                TimeSinceOrange = 0;
                ShootOrange();
            }
        }

        if (PurpleTurret)
        {
            TimeSincePurple += Time.deltaTime;

            if (TimeSincePurple > TimeBetweenShots)
            {
                TimeSincePurple = 0;
                ShootPurple();
            }
        }

        if (GreenTurret)
        {
            TimeSinceGreen += Time.deltaTime;

            if (TimeSinceGreen > TimeBetweenShots)
            {
                TimeSinceGreen = 0;
                ShootGreen();
            }
        }

    }

    private void Shoot(E_COLORS primary, E_COLORS secondary)
    {

        GM.AttackOnLine(Line, primary);

        if (secondary == E_COLORS.NONE) return;

        GM.AttackOnLine(Line, secondary);

    }

    public void ShootRed()
	{
        Shoot(E_COLORS.RED, E_COLORS.NONE);
	}

    public void ShootBlue()
    {
        Shoot(E_COLORS.BLUE, E_COLORS.NONE);
    }

    public void ShootYellow()
    {
        Shoot(E_COLORS.YELLOW, E_COLORS.NONE);
    }

    public void ShootOrange()
    {
        Instantiate(OrangeRay, LineObject.transform.position, Quaternion.identity);
        Shoot(E_COLORS.RED, E_COLORS.YELLOW);
    }

    public void ShootPurple()
    {
        Instantiate(PurpleRay, LineObject.transform.position, Quaternion.identity);
        Shoot(E_COLORS.RED, E_COLORS.BLUE);
    }

    public void ShootGreen()
    {
        Instantiate(GreenRay, LineObject.transform.position, Quaternion.identity);
        Shoot(E_COLORS.YELLOW, E_COLORS.BLUE);
    }

}

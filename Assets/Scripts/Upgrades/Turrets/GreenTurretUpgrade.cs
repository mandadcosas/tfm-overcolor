using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenTurretUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Torreta Verde";
        this.Description = "Dispara frecuentemente un rayo verde.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 24; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Torreta Verde";
        this.Description = "Dispara frecuentemente un rayo verde.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.ActivateTurret(E_COLORS.YELLOW, E_COLORS.BLUE);

    }
}

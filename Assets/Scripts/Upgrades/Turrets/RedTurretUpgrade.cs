using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedTurretUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Torreta Roja";
        this.Description = "Dispara frecuentemente un rayo rojo.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 19; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Torreta Roja";
        this.Description = "Dispara frecuentemente un rayo rojo.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.ActivateTurret(E_COLORS.RED, E_COLORS.NONE);

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueTurretUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Torreta Azul";
        this.Description = "Dispara frecuentemente un rayo azul.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 20; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Torreta Azul";
        this.Description = "Dispara frecuentemente un rayo azul.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.ActivateTurret(E_COLORS.BLUE, E_COLORS.NONE);

    }
}

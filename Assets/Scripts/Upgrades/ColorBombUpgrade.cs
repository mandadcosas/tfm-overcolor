using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBombUpgrade : Upgrade
{
    public GameObject BombButton;

    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Bomba de Color";
        this.Description = "Desbloquea un nuevo y poderoso ataque.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 17; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Bomba de Color";
        this.Description = "Desbloquea un nuevo y poderoso ataque.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        BombButton = GM.BombButton;
        BombButton.SetActive(true);
    }
}

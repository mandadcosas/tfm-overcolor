using UnityEngine;

//Clase hija de Upgrade con la funcionalidad de la mejora Amor
public class LoveUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Amor";
        this.Description = "Aumenta los puntos de vida.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 1; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Amor";
        this.Description = "Aumenta los puntos de vida.";


        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.MaxHP += 5; //Aumentamos la vida maxima del jugador
        GM.currentHP += 5;  //Aumentamos la vida actual del jugador en la misma cantidad
    }
}
using UnityEngine;

//Clase hija de Upgrade con la funcionalidad de la mejora Ultra Baterias nuevas
public class UltraBatteryUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Ultra Baterias nuevas";
        this.Description = "Reduce mucho el enfriamiento de los botones.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 4; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Ultra Baterias nuevas";
        this.Description = "Reduce mucho el enfriamiento de los botones.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.UpdateCDR(35); //Aplicamos una reduccion del enfriamiento de los botones
    }
}
using UnityEngine;

//Clase hija de Upgrade con la funcionalidad de la mejora Baterias nuevas
public class BatteryUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Baterias nuevas";
        this.Description = "Reduce el enfriamiento de los botones.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 3; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Baterias nuevas";
        this.Description = "Reduce el enfriamiento de los botones.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.UpdateCDR(15); //Aplicamos una reduccion del enfriamiento de los botones
        
    }
}

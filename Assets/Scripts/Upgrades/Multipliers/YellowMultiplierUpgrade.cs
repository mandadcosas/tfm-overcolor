using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowMultiplierUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Multiplicador Amarillo";
        this.Description = "Aumenta la cantidad de color amarillo obtenida.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 15; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Multiplicador Amarillo";
        this.Description = "Aumenta la cantidad de color amarillo obtenida.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.YellowMultiplier += 1;

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueMultiplierUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Multiplicador Azul";
        this.Description = "Aumenta la cantidad de color azul obtenida.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 14; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Multiplicador Azul";
        this.Description = "Aumenta la cantidad de color azul obtenida.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.BlueMultiplier += 1;

    }
}

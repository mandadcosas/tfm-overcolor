using UnityEngine;

//Clase hija de Upgrade con la funcionalidad de la mejora Ultra Amor
public class UltraLoveUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Ultra Amor";
        this.Description = "Aumenta mucho los puntos de vida.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 2; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Ultra Amor";
        this.Description = "Aumenta mucho los puntos de vida.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.MaxHP += 15; //Aumentamos la vida maxima del jugador
        GM.currentHP += 15; //Aumentamos la vida actual del jugador en la misma cantidad
    }
}
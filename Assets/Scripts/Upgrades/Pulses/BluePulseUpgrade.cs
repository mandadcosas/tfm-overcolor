using UnityEngine;

//Clase hija de Upgrade con la funcionalidad de la mejora Maquina de Pulsos Azul
public class BluePulseUpgrade : Upgrade
{
    public float TimeSinceLast;     //Float que almacena el tiempo desde el ultimo pulso
    public float TimeBetween;       //Float que indica cuanto tiempo ha de pasar entre pulsos
    public Animator Pulse;          //Animator que gestiona la animacion de los pulsos

    public bool Charged;            //Booleano que indica si el pulso se esta disparando o no

    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Maquina de Pulsos Azul";
        this.Description = "Crea un pulso azul de vez en cuando.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 6; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Maquina de Pulsos Azul";
        this.Description = "Crea un pulso azul de vez en cuando.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        //Pulse = GameObject.Find("PulseAnimator").GetComponent<Animator>(); //Inicializamos el Animator de los pulsos buscandolo en la escena
        transform.position = new Vector3(4, -2, 0);

        TimeBetween = 15f; //Inicializamos el tiempo entre pulsos
        TimeSinceLast = 0f; //Inicializamos el contador

        Charged = false; //Originalmente, no se esta disparando un pulso

    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        TimeSinceLast += Time.deltaTime; //Aumentamos el contador de entre interaciones

        //Si hemos superado el umbral entre pulsos y no se esta aun disparando
        if (TimeSinceLast >= TimeBetween && !Charged)
        {
            Charged = true; //Indicamos que estamos disparando
            TimeSinceLast = 0f; //Reseteamos el contador
            Pulse.Play("BluePulse"); //Reproducimos la animacion del pulso
            GM.PulseAttack(E_COLORS.BLUE); //Realizamos un ataque de pulso AZUL
        }
        //En caso de que se estuviese disparando y pase un ligero retardo
        else if (Charged && TimeSinceLast >= 1)
        {
            Charged = false; //Se termina de disparar
            TimeSinceLast = 0f; //Reseteamos el contador
            GM.PulseAttack(E_COLORS.BLUE); //Hacemos un segundo ataque AZUL para que la eliminacion de enemigos coincida con la animacion del pulso
        }
    }
}
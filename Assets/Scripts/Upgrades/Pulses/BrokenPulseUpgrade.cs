using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenPulseUpgrade : Upgrade
{
    public float TimeSinceLast;     //Float que almacena el tiempo desde el ultimo pulso
    public float TimeBetween;       //Float que indica cuanto tiempo ha de pasar entre pulsos
    public Animator Pulse;          //Animator que gestiona la animacion de los pulsos

    public bool Charged;            //Booleano que indica si el pulso se esta disparando o no

    public E_COLORS CurrentColor;

    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Maquina de Pulsos Rota";
        this.Description = "Crea un pulso aleatorio de vez en cuando.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 9; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Maquina de Pulsos Rota";
        this.Description = "Crea un pulso aleatorio de vez en cuando.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        //Pulse = GameObject.Find("PulseAnimator").GetComponent<Animator>(); //Inicializamos el Animator de los pulsos buscandolo en la escena
        transform.position = new Vector3(4, -2, 0);

        TimeBetween = 15f; //Inicializamos el tiempo entre pulsos
        TimeSinceLast = 0f; //Inicializamos el contador

        Charged = false; //Originalmente, no se esta disparando un pulso
        CurrentColor = E_COLORS.RED;

    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        TimeSinceLast += Time.deltaTime; //Aumentamos el contador de entre interaciones

        //Si hemos superado el umbral entre pulsos y no se esta aun disparando
        if (TimeSinceLast >= TimeBetween && !Charged)
        {
            //Generacion aleatoria del color
            int r = Random.Range(0, 3);

            if (r == 0)
            {
                CurrentColor = E_COLORS.RED;
                Pulse.Play("RedPulse"); //Reproducimos la animacion del pulso
            }
            else if (r == 1) {
                CurrentColor = E_COLORS.YELLOW;
                Pulse.Play("YellowPulse"); //Reproducimos la animacion del pulso
            }
			else
			{
                CurrentColor = E_COLORS.BLUE;
                Pulse.Play("BluePulse"); //Reproducimos la animacion del pulso
            }

            Charged = true; //Indicamos que estamos disparando
            TimeSinceLast = 0f; //Reseteamos el contador
            
            GM.PulseAttack(CurrentColor); //Realizamos un ataque de pulso AMARILLO
        }
        //En caso de que se estuviese disparando y pase un ligero retardo
        else if (Charged && TimeSinceLast >= 1)
        {
            Charged = false; //Se termina de disparar
            TimeSinceLast = 0f; //Reseteamos el contador
            GM.PulseAttack(CurrentColor); //Hacemos un segundo ataque AMARILLO para que la eliminacion de enemigos coincida con la animacion del pulso
        }
    }
}

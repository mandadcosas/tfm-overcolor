using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrebolUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Trebol de cuatro hojas";
        this.Description = "Aumenta la probabilidad de obtener objetos raros.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 16; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Trebol de cuatro hojas";
        this.Description = "Aumenta la probabilidad de obtener objetos raros.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.RareUpgradeChance += 20;

    }
}

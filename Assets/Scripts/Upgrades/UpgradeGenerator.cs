using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase encargada de la generacion aleatoria de Mejoras
public class UpgradeGenerator : MonoBehaviour
{
    public List<GameObject> BasicUpgrades;          //Array con todas las Mejoras disponibles en el juego
    public List<GameObject> RareUpgrades;          //Array con todas las Mejoras disponibles en el juego

    public GameplayManager GM;                      //GameplayManager de la escena
    public GameObject UpgradesCanvas;               //Canvas en el que se mostraran las Mejoras a elegir
    public GameObject ButtonCanvas;                 //Canvas con los botones del jugador (rojo, azul y amarillo)

    public UpgradeButtonController UpgradeOption1;  //Controlador de los botones para la Opcion 1 de posible Mejora
    public UpgradeButtonController UpgradeOption2;  //Controlador de los botones para la Opcion 2 de posible Mejora
    public UpgradeButtonController UpgradeOption3;  //Controlador de los botones para la Opcion 3 de posible Mejora

    public Text Upgrade1Name;                       //Texto con el nombre de la Opcion 1 de posible Mejora
    public Text Upgrade2Name;                       //Texto con el nombre de la Opcion 2 de posible Mejora
    public Text Upgrade3Name;                       //Texto con el nombre de la Opcion 3 de posible Mejora

    public Text Upgrade1Description;                //Texto con la descripcion de la Opcion 1 de posible Mejora
    public Text Upgrade2Description;                //Texto con la descripcion de la Opcion 2 de posible Mejora
    public Text Upgrade3Description;                //Texto con la descripcion de la Opcion 3 de posible Mejora

    public bool Given;                              //Booleano que indica si una mejora ya fue entregada al jugador o no en la misma seleccion

    public GameMusicManager GMusic;                 //GameMusicManager que gestiona el cambio entre canciones

    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena
        Given = false; //Originalmente no se ha entregado ninguna Mejora
        GMusic = GameObject.Find("MusicManager").GetComponent<GameMusicManager>();
    }

    public void GenerateUpgrades()
	{
        Given = true; //Indicaremos que ya se ha entregado una Mejora

        FillUpgradeMenu(); //Rellenaremos el menu con las tres opciones de Mejoras ofrecidas al jugador

        UpgradesCanvas.SetActive(true); //Mostraremos el canvas con las opciones de Mejoras
        ButtonCanvas.SetActive(false); //Ocultaremos el canvas con los botones de juego

        GMusic.PauseSong();
        Time.timeScale = 0f; //Detendremos el paso del tiempo para pausar el gameplay
    }

    //Funcion por la que se rellenan las posibles opciones de Mejoras de forma aleatoria
    public void FillUpgradeMenu()
	{
        //OPCION 1 DE MEJORA
        //Generamos un numero aleatorio en base al numero de Mejoras en el juego

        int random = 0;
        float quality = Random.Range(0, 100); //Probabilidad de obtener un objeto raro. A mas baja mejor
        bool rare1 = false; //Nos indica si el objeto encontrado es raro o no

        //Comprobamos si debemos mirar en la pool de objetos raros o en la de normales
        if(quality < GM.RareUpgradeChance)
		{
            random = Random.Range(0, RareUpgrades.Count);
            rare1 = true;
		}
		else
		{
            random = Random.Range(0, BasicUpgrades.Count);
        }

		//Obtenemos una Mejora aleatoria

		if (rare1)
		{
            UpgradeOption1.upgrade = RareUpgrades[random];
        }
		else
		{
            UpgradeOption1.upgrade = BasicUpgrades[random];
        }
        
        UpgradeOption1.upgrade.GetComponent<Upgrade>().WriteData(); //Inicializamos la informacion especifica de la Mejora
        Upgrade1Name.text = UpgradeOption1.upgrade.GetComponent<Upgrade>().Name; //Escribimos el nombre de la Mejora en la Opcion 1
        Upgrade1Description.text = UpgradeOption1.upgrade.GetComponent<Upgrade>().Description; //Escribimos la descripcion de la Mejora en la Opcion 1
        UpgradeOption1.UpdateSprite();

        //OPCION 2 DE MEJORA
        //Generamos un segundo numero aleatorio en base al numero de Mejoras en el juego
        int random2 = 0;
        float quality2 = Random.Range(0, 100); //Probabilidad de obtener un objeto raro. A mas baja mejor
        bool rare2 = false; //Nos indica si el objeto encontrado es raro o no

        //Comprobamos si debemos mirar en la pool de objetos raros o en la de normales
        if (quality2 < GM.RareUpgradeChance)
        {
            random2 = Random.Range(0, RareUpgrades.Count);
            rare2 = true;
        }
        else
        {
            random2 = Random.Range(0, BasicUpgrades.Count);
            rare2 = false;
        }

        //En caso de que el objeto coincida exactamente con la opcion de objeto 1, buscamos otra opcion diferente repitiendo el proceso
        while (random2 == random && rare1 == rare2)
		{
            quality2 = Random.Range(0, 100);

            if (quality2 < GM.RareUpgradeChance)
            {
                random2 = Random.Range(0, RareUpgrades.Count);
                rare2 = true;
            }
            else
            {
                random2 = Random.Range(0, BasicUpgrades.Count);
                rare2 = false;
            }

        }

        //Obtenemos una segunda Mejora aleatoria
        if (rare2)
        {
            UpgradeOption2.upgrade = RareUpgrades[random2];
        }
        else
        {
            UpgradeOption2.upgrade = BasicUpgrades[random2];
        }

        UpgradeOption2.upgrade.GetComponent<Upgrade>().WriteData(); //Inicializamos la informacion especifica de la Mejora
        Upgrade2Name.text = UpgradeOption2.upgrade.GetComponent<Upgrade>().Name; //Escribimos el nombre de la Mejora en la Opcion 2
        Upgrade2Description.text = UpgradeOption2.upgrade.GetComponent<Upgrade>().Description; //Escribimos la descripcion de la Mejora en la Opcion 2
        UpgradeOption2.UpdateSprite();

        //OPCION 3 DE MEJORA
        //Generamos un tercer numero aleatorio en base al numero de Mejoras en el juego
        int random3 = 0;
        float quality3 = Random.Range(0, 100); //Probabilidad de obtener un objeto raro. A mas baja mejor
        bool rare3 = false; //Nos indica si el objeto encontrado es raro o no

        //Comprobamos si debemos mirar en la pool de objetos raros o en la de normales
        if (quality3 < GM.RareUpgradeChance)
        {
            random3 = Random.Range(0, RareUpgrades.Count);
            rare3 = true;
        }
        else
        {
            random3 = Random.Range(0, BasicUpgrades.Count);
            rare3 = false;
        }

        //En caso de que el objeto coincida exactamente con la opcion de objeto 1 o la del objeto 2, buscamos otra opcion diferente repitiendo el proceso
        while ((random3 == random && rare3 == rare1) || (random3 == random2 && rare3 == rare2))
        {
            quality3 = Random.Range(0, 100); //A mas baja mejor

            if (quality3 < GM.RareUpgradeChance)
            {
                random3 = Random.Range(0, RareUpgrades.Count);
                rare3 = true;
            }
            else
            {
                random3 = Random.Range(0, BasicUpgrades.Count);
                rare3 = false;
            }

        }

        //Obtenemos una tercera Mejora aleatoria
        //Obtenemos una segunda Mejora aleatoria
        if (rare3)
        {
            UpgradeOption3.upgrade = RareUpgrades[random3];
        }
        else
        {
            UpgradeOption3.upgrade = BasicUpgrades[random3];
        }

        UpgradeOption3.upgrade.GetComponent<Upgrade>().WriteData(); //Inicializamos la informacion especifica de la Mejora
        Upgrade3Name.text = UpgradeOption3.upgrade.GetComponent<Upgrade>().Name; //Escribimos el nombre de la Mejora en la Opcion 3
        Upgrade3Description.text = UpgradeOption3.upgrade.GetComponent<Upgrade>().Description; //Escribimos la descripcion de la Mejora en la Opcion 3
        UpgradeOption3.UpdateSprite();
    }
}
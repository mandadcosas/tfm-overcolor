using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Turbo!";
        this.Description = "Vuelve todo mas frenetico.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 26; //Rellenamos el Identificador
        this.RareUpgrade = true; //La Mejora es Rara

        //Rellenamos el nombre y la descripcion
        this.Name = "Turbo!";
        this.Description = "Vuelve todo m�s frenetico.";

        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena

        float oldCD = GM.CDReduction;

        GM.UpdateCDR(90 - GM.CDReduction); //Aplicamos una reduccion del enfriamiento de los botones
        GM.MaxHP +=  (int) oldCD; //Aumentamos la vida maxima del jugador
        GM.currentHP += (int)oldCD; //Aumentamos la vida actual del jugador en la misma cantidad

        GM.EnemyRateMultiplier = 1.5f;
    }
}

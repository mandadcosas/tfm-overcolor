using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotiquinUpgrade : Upgrade
{
    //Funcion que inicializa las variables de la Mejora
    public override void WriteData()
    {
        //Rellenamos el nombre y la descripcion de la Mejora
        this.Name = "Botiquin";
        this.Description = "Cura al completo la vida.";
    }

    //Funcion llamada automaticamente al inicio de la ejecucion
    void Start()
    {
        this.ID = 8; //Rellenamos el Identificador

        //Rellenamos el nombre y la descripcion
        this.Name = "Botiquin";
        this.Description = "Cura al completo la vida.";


        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
        GM.currentHP = GM.MaxHP;
    }
}

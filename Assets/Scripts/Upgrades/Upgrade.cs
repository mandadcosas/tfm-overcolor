using UnityEngine;

//Clase base sobre la que se cosntruyen las Mejoras obtenibles durante una run
public class Upgrade : MonoBehaviour
{
    public int ID = 0;                          //Identificador de la mejora
    public bool RareUpgrade = false;            //Booleano que indica si la mejora es de calidad Rara o no
    public string Name = "Name";                //Nombre de la mejora
    public string Description = "Description";  //Descripcion de la mejora
    public GameplayManager GM;                  //GameplayManager de la escena

    //Funcion virtual a sobreescribir por la que se inicializan las variables de la Mejora
    public virtual void WriteData() { }    
}

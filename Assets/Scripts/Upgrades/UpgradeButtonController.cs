using UnityEngine;
using UnityEngine.UI;

//Clase que gestiona el comportamiento del boton asociado a seleccionar una Mejora
public class UpgradeButtonController : MonoBehaviour
{

	public GameObject upgrade;			//Mejora asociada al boton
	public GameplayManager GM;			//GameplayManager de la escena

	public GameObject UpgradesCanvas;	//Canvas con las opciones de Mejoras
	public GameObject ButtonCanvas;     //Canvas con los botones del gameplay (rojo, amarillo, azul)
	public UpgradeGenerator UG;

	public Image UpgradeSprite;         //UI Image con el sprite de la imagen

	public GameMusicManager GMusic;

	private float TimeExisted = 0;

	//Funcion llamada automaticamente al inicio de la ejecucion
	private void Start()
	{
		GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Inicializamos el GameplayManager buscandolo en la escena
		UG = GameObject.Find("UpgradeGenerator").GetComponent<UpgradeGenerator>();
		GMusic = GameObject.Find("MusicManager").GetComponent<GameMusicManager>();

		GetComponent<Button>().interactable = false;

		UpdateSprite();

	}

	private void Update()
	{
		//Evitar que el jugador pulse sin querer una mejora
		TimeExisted += Time.unscaledDeltaTime;
		if(TimeExisted > 1)
		{
			MakeButtonUsable();
		}
	}

	public void MakeButtonUsable()
	{
		GetComponent<Button>().interactable = true;
	}

	public void UpdateSprite()
	{
		UpgradeSprite.sprite = upgrade.GetComponent<SpriteRenderer>().sprite;
	}

	//Funcion llamada al pulsar el boton de la Mejora que indica que la Mejora fue escogida por el jugador
	public void ChooseThisUpgrade()
	{
		//Creamos una instancia de la Mejora y la agregamos a la lista de Mejoras en uso en el GameplayManager
		GameObject finalSelection = Instantiate(upgrade);		
		GM.UpgradeList.Add(finalSelection);

		UG.BasicUpgrades.Remove(upgrade);
		UG.RareUpgrades.Remove(upgrade);


		ButtonCanvas.SetActive(true); //Reactivamos el Canvas con los botones de juego
		Time.timeScale = 1f; //Reactivamos la escala temporal a 1 para cancelar la pausa
		UpgradesCanvas.SetActive(false); //Ocultamos el Canvas con las opciones de Mejoras
		GMusic.ContinueSong();

	}
}
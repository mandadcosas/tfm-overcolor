using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Clase que controla el proceso de pausar el juego
public class PauseController : MonoBehaviour
{
	public GameplayManager GM;				//GameplayManager
	public GameObject PauseCanvas;			//Canvas con el menu de pausa

	public Image[] UpgradesSpriteSlots;		//Array con los slots en los que colocar los sprites de las mejoras obtenidas en la run

	public GameMusicManager GMusic;			//GameMusicManager

	//Funcion llamada al comienzo de la ejecucion
	private void Start()
	{
		GMusic = GameObject.Find("MusicManager").GetComponent<GameMusicManager>();
	}

	//Funcion que coloca los sprites de los 12 ultimos objetos obtenidos en los slots del menu
	public void UpdateSprites()
	{
		if (GM.UpgradeList.Count == 0) return;

		for (int i = GM.UpgradeList.Count - 1; i >= 0 ; i--)
		{
			UpgradesSpriteSlots[i].enabled = true;
			UpgradesSpriteSlots[i].sprite = GM.UpgradeList[i].gameObject.GetComponent<SpriteRenderer>().sprite;
		}
	}

	//Funcion llamada cuando se pulsa el boton de pausa
    public void PauseButtonPressed()
	{
		PauseCanvas.SetActive(true);
		UpdateSprites();
		GMusic.PauseSong();
		Time.timeScale = 0;
		
	}

	//Funcion llamada cuando se pulsa el boton de continuar
	public void ContinueButtonPressed()
	{
		PauseCanvas.SetActive(false);
		Time.timeScale = 1;
		GMusic.ContinueSong();
	}

	//Funcion llamada cuando se pulsa el boton de volver a la villa
	public void VillageButtonPressed()
	{
		Time.timeScale = 1;
		SaveSystem.SaveGameData();
		SceneManager.LoadScene("Village");
	}
}

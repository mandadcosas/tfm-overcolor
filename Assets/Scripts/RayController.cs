using UnityEngine;

//Funcion que gestiona el comportamiento visual de los rayos disparados por el jugador al pulsar los botones
public class RayController : MonoBehaviour
{
    private float liveTime;	//Float con el tiempo de vida del rayo
    private float maxTime;  //Float con el tiempo maximo que ha de vivir el rayo

	//Funcion llamada automaticamente al comienzo de la ejecucion
	private void Start()
	{
		//Inicializamos ambos valores temporales
		liveTime = 0f;
		maxTime = 1f;
	}

	//Funcion llamada en cada frame de la ejecucion
	void Update()
    {
		liveTime += Time.deltaTime; //Aumentamos el contador del tiempo transcurrido

		//En caso de alcanzarse el tiempo maximo de vida, se destruye la instancia del rayo
		if(liveTime > maxTime)
		{
			Destroy(gameObject);
		}       
    }
}
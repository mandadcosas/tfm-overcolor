using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadingController : MonoBehaviour
{
    // Start is called before the first frame update

    private float counter = 0;


    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        if (counter >= 1.2) Destroy(gameObject);
    }
}

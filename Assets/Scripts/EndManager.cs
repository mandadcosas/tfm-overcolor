using UnityEngine;
using UnityEngine.UI;

//Clase que inicializa los textos de la pantalla de final de partida
public class EndManager : MonoBehaviour
{
    public Text TimeText;                   //Texto con el tiempo de juego
    public MeshRenderer Background;         //MeshRenderer del fondo

    public float TimeInEnd = 0;             //Tiempo transcurrido en la escena de fin
    public float Adjuster = 1;              //Parametro para ajustar la velocidad del fondo

    public Transform EndRoloc;              //Transform del sprite de roloc que cae desde el cielo 
    public bool AnimEnded = false;          //Booleano que indica si roloc ya termino de caer

    public GameObject ButtonReplay;         //GameObject con el boton de volver a jugar
    public GameObject ButtonVillage;        //GameObject con el boton de volver a la villa

    public AudioSource FallingSound;        //Sonido de caer

    private bool FallingActivated = false;  //Booleano que indica si ya se reprodujo el sonido de caer

    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        TimeText.text = (int)EndValues.Time + " s"; //Editamos el texto con el tiempo transcurrido en la partida  
        SaveSystem.SaveGameData();
    }

    //Funcion llamada en cada frame
	private void Update()
	{
        //El temporizador siempre va subiendo
        TimeInEnd += Time.deltaTime;

        //Ajustamos el fondo para que se mueva indefinidamente
        Vector2 offset = new Vector2(0, -1 * TimeInEnd / Adjuster);
        Background.material.mainTextureOffset = offset;

        if (AnimEnded) return; //Si roloc termino de caer, no se hace nada mas

        if(TimeInEnd > 10) //Si pasase demasiado tiempo, activamos los botones
		{
            AnimEnded = true;
            ButtonReplay.SetActive(true);
            ButtonVillage.SetActive(true);
        }

        if(EndRoloc.position.y < 5 && !FallingActivated) //Cuando roloc empiece a entrar por pantalla reproducimos el sonido de caida
		{
            FallingActivated = true;
            FallingSound.Play();
        }

        if(EndRoloc.position.y < -15) //Cuando roloc termine de caer, activamos los botones y destruimos a roloc
		{
            AnimEnded = true;
            ButtonReplay.SetActive(true);
            ButtonVillage.SetActive(true);

            Destroy(EndRoloc.gameObject);
        }
    }
}
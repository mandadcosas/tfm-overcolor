using UnityEngine;

//Clase hija de Building que contiene la funcionalidad del edificio Casa
public class Casa : Building
{
	//Funcion que inicializa las variables del objeto con los valores para Casa
	public override void FillData()
	{
		//Insertamos los valores a mostrar en el HUD
		BuildingName = "Casa";

		Profits = "+ 5 Puntos de Vida";

		Price1Color = E_COLORS.RED;

		MaxLevel = 3; //Fijamos el numero maximo de niveles del edificio

		BuildMenuController.currentBuilding = this; //Indicamos que el PopUp debe mostrar los datos de Casa

		//En base al nivel en el que se encuentre la casa, el precio de subida de nivel sera mayor
		if (BuildingLevels.CasaLevel == 0)
		{
			Price1 = 300;
		}
		else if (BuildingLevels.CasaLevel == 1)
		{
			Price1 = 1000;
		}
		else if (BuildingLevels.CasaLevel == 2)
		{
			Price1 = 3000;
		}
		
		//En caso de que no tengamos suficiente divisa como para pagar la mejora, el boton de Aceptar no sera interactuable
		if (Currencies.RedCurrency < Price1)
		{
			BuildMenuController.AcceptB.interactable = false;
		}
		//En caso de tener suficiente divisa, nos aseguramos de que este activo
		else
		{
			BuildMenuController.AcceptB.interactable = true;
		}

		FillButton(); //Rellenamos el PopUp con los datos asignados a la Casa

		if (BuildingLevels.CasaLevel >= MaxLevel)
		{
			FillFullyUpgraded();
		}

		BuildMenuController.PopUpCanvas.SetActive(true); //Activamos el PopUp para que sea visible al jugador
	}

	//Funcion que gestiona la subida de nivel del edificio
	public override void LevelBuilding()
	{
		//Nos aseguramos de tener suficiente divisa para hacer la mejora
		if (Currencies.RedCurrency >= Price1)
		{
			Currencies.RedCurrency -= Price1; //Reducimos la divisa en base al precio
			BuildingLevels.CasaLevel += 1; //Aumentamos el nivel del edificio
			ApplyBuff(); //Aplicamos los beneficios de subir de nivel
		}
	}

	//Funcion que aplica el beneficio de mejorar el edificio
	public override void ApplyBuff()
	{
		PlayerStats.MaxHP += 5; //Aumentamos la vida maxima del jugador
	}

	//Funcion que rellena el contenido del PopUp con los datos del edificio
	public override void FillButton()
	{
		base.FillButton(); //Llamamos a la funcionalidad basica para todos los edificios
		Price1Text.text = Price1 + " Rojo"; //Rellenamos el campo correspondiente al precio y su color
		Price1Text.color = new Color(255, 0, 0); //Cambiamos el color del texto para que sea rojo, igual que el precio
	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bosque : Building
{
	//Funcion que inicializa las variables del objeto con los valores para Bosque
	public override void FillData()
	{
		//Insertamos los valores a mostrar en el HUD
		BuildingName = "Bosque";

		Profits = "+ 10% Probabilidad de encontrar objetos raros";

		Price1Color = E_COLORS.YELLOW;
		Price2Color = E_COLORS.BLUE;

		MaxLevel = 3; //Fijamos el numero maximo de niveles del edificio

		BuildMenuController.currentBuilding = this; //Indicamos que el PopUp debe mostrar los datos de Bosque

		//En base al nivel en el que se encuentre el bosque, el precio de subida de nivel sera mayor
		if (BuildingLevels.BosqueLevel == 0)
		{
			Price1 = 750;
			Price2 = 750;
		}
		else if (BuildingLevels.BosqueLevel == 1)
		{
			Price1 = 2500;
			Price2 = 2500;
		}
		else if (BuildingLevels.BosqueLevel == 2)
		{
			Price1 = 5000;
			Price2 = 5000;
		}

		//En caso de que no tengamos suficiente divisa como para pagar la mejora, el boton de Aceptar no sera interactuable
		if (Currencies.YellowCurrency < Price1 || Currencies.BlueCurrency < Price2)
		{
			BuildMenuController.AcceptB.interactable = false;
		}
		//En caso de tener suficiente divisa, nos aseguramos de que este activo
		else
		{
			BuildMenuController.AcceptB.interactable = true;
		}

		FillButton(); //Rellenamos el PopUp con los datos asignados a Bosque

		if (BuildingLevels.BosqueLevel >= MaxLevel)
		{
			FillFullyUpgraded();
		}

		BuildMenuController.PopUpCanvas.SetActive(true); //Activamos el PopUp para que sea visible al jugador
	}

	//Funcion que gestiona la subida de nivel del edificio
	public override void LevelBuilding()
	{
		//Nos aseguramos de tener suficiente divisa para hacer la mejora
		if (Currencies.YellowCurrency >= Price1 && Currencies.BlueCurrency >= Price2)
		{
			Currencies.YellowCurrency -= Price1; //Reducimos la divisa en base al precio
			Currencies.BlueCurrency -= Price2; //Reducimos la divisa en base al precio

			BuildingLevels.BosqueLevel += 1; //Aumentamos el nivel del edificio
			ApplyBuff(); //Aplicamos los beneficios de subir de nivel
		}
	}

	//Funcion que aplica el beneficio de mejorar el edificio
	public override void ApplyBuff()
	{
		PlayerStats.RareChance += 10; //Aumentamos la vida maxima del jugador
	}

	//Funcion que rellena el contenido del PopUp con los datos del edificio
	public override void FillButton()
	{
		base.FillButton(); //Llamamos a la funcionalidad basica para todos los edificios
		Price1Text.text = Price1 + " Amarillo"; //Rellenamos el campo correspondiente al precio y su color
		Price1Text.color = new Color32(196, 189, 48, 255); //Cambiamos el color del texto para que sea rojo, igual que el precio

		Price2Text.text = Price2 + " Azul"; //Rellenamos el campo correspondiente al precio y su color
		Price2Text.color = new Color(0, 0, 255); //Cambiamos el color del texto para que sea rojo, igual que el precio
	}
}

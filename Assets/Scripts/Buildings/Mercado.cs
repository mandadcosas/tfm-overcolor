using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mercado : Building
{
	//Funcion que inicializa las variables del objeto con los valores para Mercado
	public override void FillData()
	{
		//Insertamos los valores a mostrar en el HUD
		BuildingName = "Mercado";

		Profits = "Comienza el acceso con +1 objeto extra";

		Price1Color = E_COLORS.YELLOW;
		Price2Color = E_COLORS.RED;

		MaxLevel = 3; //Fijamos el numero maximo de niveles del edificio

		BuildMenuController.currentBuilding = this; //Indicamos que el PopUp debe mostrar los datos de Mercado

		//En base al nivel en el que se encuentre el mercado, el precio de subida de nivel sera mayor
		if (BuildingLevels.MercadoLevel == 0)
		{
			Price1 = 1000;
			Price2 = 1000;
		}
		else if (BuildingLevels.MercadoLevel == 1)
		{
			Price1 = 3000;
			Price2 = 3000;
		}
		else if (BuildingLevels.MercadoLevel == 2)
		{
			Price1 = 6000;
			Price2 = 6000;
		}

		//En caso de que no tengamos suficiente divisa como para pagar la mejora, el boton de Aceptar no sera interactuable
		if (Currencies.YellowCurrency < Price1 || Currencies.RedCurrency < Price2)
		{
			BuildMenuController.AcceptB.interactable = false;
		}
		//En caso de tener suficiente divisa, nos aseguramos de que este activo
		else
		{
			BuildMenuController.AcceptB.interactable = true;
		}

		FillButton(); //Rellenamos el PopUp con los datos asignados a Mercado

		if (BuildingLevels.MercadoLevel >= MaxLevel)
		{
			FillFullyUpgraded();
		}

		BuildMenuController.PopUpCanvas.SetActive(true); //Activamos el PopUp para que sea visible al jugador
	}

	//Funcion que gestiona la subida de nivel del edificio
	public override void LevelBuilding()
	{
		//Nos aseguramos de tener suficiente divisa para hacer la mejora
		if (Currencies.YellowCurrency >= Price1 && Currencies.RedCurrency >= Price2)
		{
			Currencies.YellowCurrency -= Price1; //Reducimos la divisa en base al precio
			Currencies.RedCurrency -= Price2; //Reducimos la divisa en base al precio

			BuildingLevels.MercadoLevel += 1; //Aumentamos el nivel del edificio
			ApplyBuff(); //Aplicamos los beneficios de subir de nivel
		}
	}

	//Funcion que aplica el beneficio de mejorar el edificio
	public override void ApplyBuff()
	{
		PlayerStats.StartingItems += 1; //Aumentamos la vida maxima del jugador
	}

	//Funcion que rellena el contenido del PopUp con los datos del edificio
	public override void FillButton()
	{
		base.FillButton(); //Llamamos a la funcionalidad basica para todos los edificios
		Price1Text.text = Price1 + " Amarillo"; //Rellenamos el campo correspondiente al precio y su color
		Price1Text.color = new Color32(196, 189, 48, 255); //Cambiamos el color del texto para que sea rojo, igual que el precio

		Price2Text.text = Price2 + " Rojo"; //Rellenamos el campo correspondiente al precio y su color
		Price2Text.color = new Color(255, 0, 0); //Cambiamos el color del texto para que sea rojo, igual que el precio
	}
}

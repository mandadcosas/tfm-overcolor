using UnityEngine;

//Clase hija de Building que contiene la funcionalidad del edificio Carretera
public class Carretera : Building
{
	public BuildingUnlocker BU;

	//Funcion que inicializa las variables del objeto con los valores para Carretera
	public override void FillData()
	{
		//Insertamos los valores a mostrar en el HUD

		BuildingName = "Carretera";

		Profits = "Desbloquea nuevo edificio";

		Price1Color = E_COLORS.BLUE;

		MaxLevel = 3; //Fijamos el numero maximo de niveles del edificio

		BuildMenuController.currentBuilding = this; //Indicamos que el PopUp debe mostrar los datos de Carretera

		//En base al nivel en el que se encuentre la carretera, el precio de subida de nivel sera mayor
		if (BuildingLevels.CarreterasLevel == 0)
		{
			Price1 = 500;
		}
		else if (BuildingLevels.CarreterasLevel == 1)
		{
			Price1 = 1250;
		}
		else if (BuildingLevels.CarreterasLevel == 2)
		{
			Price1 = 4000;
		}

		//En caso de que no tengamos suficiente divisa como para pagar la mejora, el boton de Aceptar no sera interactuable
		if(Currencies.BlueCurrency < Price1)
		{
			BuildMenuController.AcceptB.interactable = false;
		}
		//En caso de tener suficiente divisa, nos aseguramos de que este activo
		else
		{
			BuildMenuController.AcceptB.interactable = true;
		}

		FillButton(); //Rellenamos el PopUp con los datos asignados a la Carretera

		if (BuildingLevels.CarreterasLevel >= MaxLevel)
		{
			FillFullyUpgraded();
		}

		BuildMenuController.PopUpCanvas.SetActive(true); //Activamos el PopUp para que sea visible al jugador
	}

	//Funcion que gestiona la subida de nivel del edificio
	public override void LevelBuilding()
	{
		//Nos aseguramos de tener suficiente divisa para hacer la mejora
		if (Currencies.BlueCurrency >= Price1)
		{
			Currencies.BlueCurrency -= Price1; //Reducimos la divisa en base al precio
			BuildingLevels.CarreterasLevel += 1; //Aumentamos el nivel del edificio
			ApplyBuff();
		}
	}

	//Funcion que aplica el beneficio de mejorar el edificio
	public override void ApplyBuff()
	{
		//Desbloquear otros edificios (WIP)
		PlayerStats.UnlockedBuildings += 1;
		BU.UnlockBuildings();
	}

	//Funcion que rellena el contenido del PopUp con los datos del edificio
	public override void FillButton()
	{
		base.FillButton(); //Llamamos a la funcionalidad basica para todos los edificios
		Price1Text.text = Price1 + " Azul"; //Rellenamos el campo correspondiente al precio y su color
		Price1Text.color = new Color(0, 0, 255); //Cambiamos el color del texto para que sea azul, igual que el precio
	}
}

using UnityEngine;
using UnityEngine.UI;

//Clase base sobre la que se construiran las clases de los edificios de la villa
public class Building : MonoBehaviour
{

    public string BuildingName;     //Nombre del edificio

    public int Price1;              //Primer precio de mejora del edificio
    public int Price2;              //Segundo posible precio de mejora del edificio

    public string Profits;          //Cadena de texto con los beneficios que otorga mejorar el edificio

    public E_COLORS Price1Color;    //Color de la divisa del primer precio
    public E_COLORS Price2Color;    //Color de la divisa del posible segundo precio

    public int MaxLevel;            //Nivel maximo que podra alcanzar el edificio

    public BuildMenuButtonController BuildMenuController; //Controlador de los botones del PopUp

    public Text NameText;           //Texto de la UI del PopUp con el nombre del edificio
    public Text Price1Text;         //Texto de la UI del PopUp con el precio de mejora
    public Text Price2Text;         //Texto de la UI del PopUp con el precio de mejora
    public Text ProfitText;         //Texto de la UI del PopUp con el beneficio que otorga al mejorar el edificio

    public Text FixedText1;         //Texto fijo
    public Text FixedText2;         //Segundo texto fijo


    //FUNCIONES VIRTUALES A SER SOBREESCRITAS

    //Funcion para aplicar el beneficio de mejorar el edificio
    public virtual void ApplyBuff()
	{

	}

    //Funcion para subir de nivel el edificio
    public virtual void LevelBuilding()
    {

    }

    //Funcion para inicializar las variables del edificio
    public virtual void FillData()
	{

	}

    //Funcion para rellenar la UI del PopUp
    public virtual void FillButton()
	{
        NameText.text = BuildingName;
        ProfitText.text = Profits;
        Price2Text.text = "";

	}

    //Funcion para rellenar la UI de un edificio completamente mejorado
    public virtual void FillFullyUpgraded()
	{
        FixedText1.text = "�Edificio mejorado al maximo!";
        ProfitText.text = "";
        Price1Text.text = "";
        Price2Text.text = "";
        FixedText2.text = "";
        BuildMenuController.AcceptB.interactable = false;
    }
}
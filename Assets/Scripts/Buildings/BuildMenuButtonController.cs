using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Clase que controla el comportamiento de los botones de la pantalla de la villa
public class BuildMenuButtonController : MonoBehaviour
{

	public Building currentBuilding;		//Edificio sobre el que se ha abierto el PopUp

	public GameObject PopUpCanvas;			//Canvas con la UI del PopUp

	public Text RedColor;					//Texto que muestra la cantidad de divisa roja en posesion
	public Text BlueColor;                  //Texto que muestra la cantidad de divisa azul en posesion
	public Text YellowColor;                //Texto que muestra la cantidad de divisa amarilla en posesion

	public Button AcceptB;                  //Boton del PopUp asociado a aceptar la mejora del edificio

	public GameObject VictoryCounter;       //GameObject con el contador de veces que el jugador ha ganado

	public AudioSource BuildingUpgradeSound; //Sonido de mejorar edificio

	//Funcion asociada al boton de aceptar la mejora del edificio
	public void AcceptButton()
	{
		currentBuilding.LevelBuilding(); //Subimos el nivel del edificio
		RefreshUI(); //Refrescamos la UI para representar el gasto de la divisa
		PopUpCanvas.SetActive(false); //Cerramos el PopUp
		BuildingUpgradeSound.Play();

		SaveSystem.SaveGameData();
	}

	//Funcion asociada al boton de cancelar del PopUp
	public void CancelButton()
	{
		PopUpCanvas.SetActive(false); //Cerramos el PopUp
	}

	//Funcion asociada al boton de Jugar
	public void PlayButton()
	{
		SceneManager.LoadScene("Game"); //Cambiamos la escena a la escena con el gameplay
	}

	//Funcion que actualiza el HUD para representar los valores actuales de las divisas
	public void RefreshUI()
	{
		RedColor.text = Currencies.RedCurrency.ToString();
		BlueColor.text = Currencies.BlueCurrency.ToString();
		YellowColor.text = Currencies.YellowCurrency.ToString();
	}

	//Funcion llamada automaticamente al comienzo de la ejecucion
	private void Start()
	{
		RefreshUI(); //Lo primero que hacemos es mostrar las divisas en la UI

		if(PlayerStats.Victories > 0)
		{
			VictoryCounter.SetActive(true);
			VictoryCounter.GetComponent<Text>().text = "Victorias: " + PlayerStats.Victories;
		}
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que gestiona el desbloqueo de edificio
public class BuildingUnlocker : MonoBehaviour
{
    public GameObject BosqueButton;     //Boton correspondiente al Bosque
    public GameObject MercadoButton;    //Boton correspondiente al Mercado
    public GameObject AeropuertoButton; //Boton correspondiente al Aeropuerto

    // Start is called before the first frame update
    void Start()
    {
        UnlockBuildings();
    }

    //Funcion que desbloquea un nuevo edificio en base a los ya desbloqueados
    public void UnlockBuildings()
	{
        if(PlayerStats.UnlockedBuildings >= 1)
		{
            MercadoButton.SetActive(true);
		}

        if (PlayerStats.UnlockedBuildings >= 2)
        {
            BosqueButton.SetActive(true);
        }

        if (PlayerStats.UnlockedBuildings >= 3)
        {
            AeropuertoButton.SetActive(true);
        }
    }
}

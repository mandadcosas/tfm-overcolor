using UnityEngine;

//Clase que gestiona el comportamiento de los enemigos basicos
public class EnemyController : MonoBehaviour
{

    public E_COLORS enemyColor = E_COLORS.RED;      //Enumerado con el Color del enemigo
    public E_COLORS enemySecondaryColor = E_COLORS.NONE;

    public GameplayManager GM;                      //GameplayManager de la escena
    public float BaseSpeed = 1.5f;                  //Velocidad base a la que se mueven los enemigos

    public bool Hitted;                             //Booleano que indica si el enemigo ha golpeado al jugador o no
    public int CurrencyValue = 5;                   //Cantidad de divisa de su color que el enemigo otorgara al jugador

    public int Line = 1;                            //Linea en la que se encuentra el enemigo

    public E_COLORS ShieldColor = E_COLORS.NONE;    //Color del escudo del enemigo, NONE implica que no hay escudo
    public bool ExplosiveEnemy = false;             //Bool que indica si el enemigo es explosivo o no
    public bool ShooterEnemy = false;               //Bool que indica si el enemigo dispara o no
    public bool Underground = false;                //Bool que indica si el enemigo es intangible/esta bajo tierra o no
    public bool JumperEnemy = false;                //Bool que indica si el enemigo salta entre lineas o no

    public GameObject ExplosionCanvas;              //Canvas con el contador de tiempo antes de la explosion
    public GameObject BulletPrefab;                 //Prefab de la bala disparable
    public GameObject HeartPrefab;                  //Prefab de los corazones dropeables

    public bool JustSpawned = true;                 //Booleano que indica si el enemigo ha spawneado este frame
    public GameObject Gun;                          //Prefab de la pistola de los enemigos disparadores
    public GameObject FadeImage;                    //Prefab de la imagen dejada por los enemigos saltarines al saltar
    public GameObject ShieldMask;                   //Prefab de la mascara de los enemigos acorazados

    public GameObject DeathParticle;            //Prefab del sistema de particulas que deja un enemigo al morir
    public GameObject ShieldDropSounder;           //Prefab de GameObject que reproduce un sonido cuando un enemigo pierde su escudo


    //Funcion llamada al instanciarse el objeto
	private void Awake()
	{
        //Instanciamos aqui el GM para evitar problemas con los enemigos spawneados tras vencer a un enemigo bicolor
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena
    }

	//Funcion llamada automaticamente al comienzo de la ejecucion
	void Start()
    {
        BaseSpeed = GameObject.Find("ZoneManager").GetComponent<ZoneManager>().CurrentZone.Speed;
        
        Hitted = false; //Originalmente el enemigo no habran golpeado al jugador

		if (ShooterEnemy)
		{     
            ShooterEnemy sh = gameObject.AddComponent<ShooterEnemy>();
            sh.MyEnemy = this;
            Gun.SetActive(true);
        }

		if (Underground)
		{
            //Color c = GetComponent<SpriteRenderer>().color;
            //c = new Color(c.r, c.g, c.b, 0.5f);
            //GetComponent<SpriteRenderer>().color = c;

            GetComponent<Animator>().SetTrigger("Hide");
        }

		if (JumperEnemy)
		{
            JumperEnemy jum = gameObject.AddComponent<JumperEnemy>();
            jum.MyEnemy = this;
            jum.GM = GM;
        }

        if(ShieldColor != E_COLORS.NONE)
		{
            ShieldMask.SetActive(true);

            if(ShieldColor == E_COLORS.RED) ShieldMask.GetComponent<SpriteRenderer>().color = new Color32(172, 50, 50, 255);
            if (ShieldColor == E_COLORS.BLUE) ShieldMask.GetComponent<SpriteRenderer>().color = new Color32(99, 155, 255, 255);
            if (ShieldColor == E_COLORS.YELLOW) ShieldMask.GetComponent<SpriteRenderer>().color = new Color32(246, 240, 113, 255);
        }
    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {
        //Evitar bug enemigo muere antes de spawnear
		if (JustSpawned)
		{
            JustSpawned = false;
            return;
		}

        if (transform.position.x <= 1 && ShooterEnemy) return;
       

        if (transform.position.x <= 1 && Underground)
        {
            Underground = false;
            //Color c = GetComponent<SpriteRenderer>().color;
            //c = new Color(c.r, c.g, c.b, 1f);
            //GetComponent<SpriteRenderer>().color = c;

            GetComponent<Animator>().SetTrigger("Normal");
        }

        //El enemigo avanzara por su linea en direccion al jugador
        //La velocidad de los enemigos ira escalando ligeramente con el tiempo de partida
        transform.Translate(Vector3.left * Time.deltaTime * BaseSpeed  * GM.EnemyRateMultiplier);

        //En caso de que el enemigo alcance la posicion del jugador y no le haya golpeado aun
        if(transform.position.x < GM.Roloc.transform.position.x && !Hitted)
		{
            Hitted = true; //Indicaremos que el enemigo ya ha golpeado al jugador
            ExplosiveEnemy = false;
            GM.DamageRoloc(); //Se realizara la penalizacion del jugador
            GetComponent<SpriteRenderer>().enabled = false; //El enemigo dejara de ser visible a la espera de su destruccion
		}
    }

    //Funcion por la que el enemigo es derrotado a manos del jugador
    //Recibe un parametro color equivalente al color del ataque recibido
    //Devuelve booleano que indica si el enemigo fue derrotado o no
    public virtual bool KillByPlayer(E_COLORS color)
	{
        if (!CheckShield(color)) return false;
        if (Underground) return false;

        //En caso de que el color del enemigo coincida con el color del ataque
        if(enemyColor == color || Hitted)
		{
            ManageDestruction();

            //En base al color del enemigo, el jugador recibira divisa de ese color
            if (color == E_COLORS.RED)
			{
                Currencies.RedCurrency += (CurrencyValue * GM.RedMultiplier);
			}
            else if (color == E_COLORS.BLUE)
            {
                //GM =  GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
                Currencies.BlueCurrency += (CurrencyValue * GM.BlueMultiplier);
            }
            else if (color == E_COLORS.YELLOW)
            {
                Currencies.YellowCurrency += (CurrencyValue * GM.YellowMultiplier);
            }

            SpawnHeart();
            return true; //Indicamos que el enemigo fue derrotado
		}
        //En caso contrario
		else
		{
            return false; //Indicamos que el enemigo no fue derrotado
		}     
	}

    //Funcion que se asegura de comprobar si el enemigo tiene un escudo capaz de deterner el ataque inmediato
    public bool CheckShield(E_COLORS color)
	{
        //Escudo
        if (ShieldColor == color)
        {
            Instantiate(ShieldDropSounder);

            ShieldColor = E_COLORS.NONE;
            ShieldMask.SetActive(false);
            return false;
        }

        if (ShieldColor != E_COLORS.NONE) return false;

        return true;
    }

    //Funcion que gestiona la destruccion del enemigo para tener en cuenta casos particulares
    public virtual void ManageDestruction()
	{
        if(enemyColor == E_COLORS.RED && enemySecondaryColor == E_COLORS.NONE)
		{
            GameObject deathP = Instantiate(DeathParticle, new Vector3(transform.position.x, transform.position.y, 0.3f), Quaternion.identity);
            deathP.GetComponent<ParticleSystem>().startColor = Color.red;
            deathP.GetComponent<ParticleSystem>().Play();
		}
        else if (enemyColor == E_COLORS.BLUE && enemySecondaryColor == E_COLORS.NONE)
		{
            GameObject deathP = Instantiate(DeathParticle, new Vector3(transform.position.x, transform.position.y, 0.3f), Quaternion.identity);
            deathP.GetComponent<ParticleSystem>().startColor = Color.blue;
            deathP.GetComponent<ParticleSystem>().Play();
        }
        else if (enemyColor == E_COLORS.YELLOW && enemySecondaryColor == E_COLORS.NONE)
		{
            GameObject deathP = Instantiate(DeathParticle, new Vector3(transform.position.x, transform.position.y, 0.3f), Quaternion.identity);
            deathP.GetComponent<ParticleSystem>().startColor = Color.yellow;
            deathP.GetComponent<ParticleSystem>().Play();
        }

            if (ExplosiveEnemy)
		{
            SetUpExplosion();
            return;
		}

        Destroy(gameObject, 0.01f); //El enemigo sera destruido al cabo de un ligero retardo para evitar bugs de sincronizacion       
    }

    //Funcion que inicializa el proceso de explosion de un enemigo
    public void SetUpExplosion()
	{
        GameObject childExplosion = Instantiate(ExplosionCanvas, transform.position, Quaternion.identity);
        childExplosion.GetComponent<EnemyExplosion>().GM = GM;
        childExplosion.GetComponent<EnemyExplosion>().Line = Line;
        childExplosion.transform.SetParent(this.transform);

        GetComponent<SpriteRenderer>().sortingLayerName = "Bomb";
        GetComponent<Animator>().SetTrigger("BombSet");
        this.enabled = false;
    }

    //Funcion por la que de forma aleatoria se spawnea un corazon
    public void SpawnHeart()
	{
        int r = Random.Range(0, GM.HeartRate);

        if(r == 0)
		{
            HeartController heart = Instantiate(HeartPrefab, transform.position, Quaternion.identity).GetComponent<HeartController>();
            heart.Line = Line;
        }
        
	}
}
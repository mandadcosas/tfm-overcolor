using UnityEngine;

//Clase que gestiona la generacion aleatoria de enemigos
public class EnemyGenerator : MonoBehaviour
{
    public GameplayManager GM;          //GameplayManager de la escena

    public float MinTimeBetweenSpawns;     //Float con el tiempo que ha de pasar entre la generacion de enemigos
    public float TimeSinceLastSpawn;    //Float con el tiempo que paso desde la generacion del ultimo enemigo

    public GameObject RedEnemy;         //Prefab del enemigo basico rojo
    public GameObject BlueEnemy;        //Prefab del enemigo basico azul
    public GameObject YellowEnemy;      //prefab del enemigo basico amarillo

    public GameObject PurpleEnemy;      //Prefab del enemigo morado
    public GameObject OrangeEnemy;      //prefab del enemigo naranja
    public GameObject GreenEnemy;       //Prefab del enemigo verde

    public GameObject DarkRedEnemy;     //prefab del enemigo rojo oscuro
    public GameObject DarkBlueEnemy;    //prefab del enemigo azul oscuro
    public GameObject DarkYellowEnemy;  //Prefab del enemigo amarillo oscuro

    public bool StopGeneration = false; //Booleano que indica si deberia detenerse la generacion de enemigos

    public Zone CurrentZone;            //Zona actual en la que se encuentra el jugador


    //Funcion llamada automaticamente al comienzo de la ejecucion
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena

        //Inicializamos las variables correspondientes al tiempo entre spawns
        MinTimeBetweenSpawns = 0.1f;
        TimeSinceLastSpawn = 0f;
    }

    //Funcion llamada en cada frame de la ejecucion
    void Update()
    {

        if (StopGeneration)
		{
            TimeSinceLastSpawn = 0f;
            return;
        }

        TimeSinceLastSpawn += Time.deltaTime; //Aumentamos el tiempo transcurrido entre generaciones

        //En caso de que el tiempo desde el ultimo spawn supere el tiempo establecido entre spawns
        if(TimeSinceLastSpawn > MinTimeBetweenSpawns)
		{
            TimeSinceLastSpawn = 0f; //Reiniciamos el contador
            float now = Random.Range(0,100); //Obtenemos un valor aleatorio

            //if(now < 10 * GM.EnemyRateMultiplier + GM.TimeTotal / 60)

            //En base al ratio de generacion de la zona generamos enemigos
            //Este ratio aumenta si tenemos alguna mejora como Turbo
            if(now < GM.EnemyRateMultiplier * CurrentZone.SpawnRate )
			{
                SpawnBasicEnemy();
			}      
        }
    }

    //Funcion por la que se genera un enemigo basico
    public void SpawnBasicEnemy()
	{
        //Obtendremos 2 numeros aleatorios
        int lineRandom = Random.Range(1, 4);
        int colorRandom = Random.Range(1, 4);

        GameObject prefabToUse = null; //GameObject que almacenara el prefab del enemigo a usar

        EnemyController enemy = null;

        //Determinamos si el enemigo sera de un color combinado/oscuro mediante azar
        //Ambas opciones son mutuamente exclusivas

        bool shouldDouble = false;
        bool shouldDark = false;

        float doubleCheck = Random.Range(0, 100);
        float darkCheck = Random.Range(0, 100 - CurrentZone.DoubleColorChance);

        if (doubleCheck < CurrentZone.DoubleColorChance)
		{
            
            shouldDouble = true;
            shouldDark = false;		
		}
		else if (darkCheck < CurrentZone.DarkColorChance)
        {
            shouldDouble = false;
            shouldDark = true;
        }
     

        //En base al segundo numero aleatorio generado, decidiremos el color del enemigo a generar
        if(colorRandom == 1)
		{
            prefabToUse = RedEnemy;

            if (shouldDouble) prefabToUse = OrangeEnemy;
            if (shouldDark) prefabToUse = DarkRedEnemy;
        }
        else if(colorRandom == 2)
		{
            prefabToUse = BlueEnemy;

            if (shouldDouble) prefabToUse = PurpleEnemy;
            if (shouldDark) prefabToUse = DarkBlueEnemy;
        }
		else if (colorRandom == 3)
        {
            prefabToUse = YellowEnemy;

            if (shouldDouble) prefabToUse = GreenEnemy;
            if (shouldDark) prefabToUse = DarkYellowEnemy;
        }
		

        //En base al primer numero aleatorio generado, decidiremos la linea en la que se generara el enemigo
        if(lineRandom == 1)
		{
            //La posicion del enemigo coincidira con la correspondiente a su linea y agregaremos una instancia a la lista de enemigos del GameplayManager
            Vector3 newPosition = new Vector3(prefabToUse.transform.position.x, GM.Line1.transform.position.y, prefabToUse.transform.position.z);
            enemy = Instantiate(prefabToUse, newPosition, Quaternion.identity).GetComponent<EnemyController>();
            
            enemy.Line = 1;
            GM.EnemiesLine1.Add(enemy);
        }
        else if (lineRandom == 2)
        {
            //La posicion del enemigo coincidira con la correspondiente a su linea y agregaremos una instancia a la lista de enemigos del GameplayManager
            Vector3 newPosition = new Vector3(prefabToUse.transform.position.x, GM.Line2.transform.position.y, prefabToUse.transform.position.z);
            enemy = Instantiate(prefabToUse, newPosition, Quaternion.identity).GetComponent<EnemyController>();
            
            enemy.Line = 2;
            GM.EnemiesLine2.Add(enemy);
        }
        else
        {
            //La posicion del enemigo coincidira con la correspondiente a su linea y agregaremos una instancia a la lista de enemigos del GameplayManager
            Vector3 newPosition = new Vector3(prefabToUse.transform.position.x, GM.Line3.transform.position.y, prefabToUse.transform.position.z);
            enemy = Instantiate(prefabToUse, newPosition, Quaternion.identity).GetComponent<EnemyController>();
            
            enemy.Line = 3;
            GM.EnemiesLine3.Add(enemy);
        }

        AddPerks(enemy); //Agregamos las posibles particularidades del enemigo
    }
   
    //Funcion para spawnear un enemigo basico especifico en una posicion especifica
    //Usado por los enemigos bicolor para generar otro enemigo una vez mueren
    public void SpawnSpecificEnemy(GameObject prefab, int line, Vector3 position)
	{
        if (line == 1)
        {
            EnemyController enemy = Instantiate(prefab, position, Quaternion.identity).GetComponent<EnemyController>();
            enemy.GM = GM;
            GM.EnemiesLine1.Add(enemy);
        }
        else if (line == 2)
        {
            EnemyController enemy = Instantiate(prefab, position, Quaternion.identity).GetComponent<EnemyController>();
            enemy.GM = GM;
            GM.EnemiesLine2.Add(enemy);
        }
        else
        {
            EnemyController enemy = Instantiate(prefab, position, Quaternion.identity).GetComponent<EnemyController>();
            enemy.GM = GM;
            GM.EnemiesLine3.Add(enemy);
        }
    }

    //Funcion por la que agregamos a los enemigos tanto la velocidad como otras particularidades de forma aleatoria
    public void AddPerks(EnemyController enemy)
    {
        enemy.BaseSpeed = CurrentZone.Speed;

        if (AddShield(enemy)) return;
        if (AddExplosive(enemy)) return;
        if (AddShooter(enemy)) return;
        if (AddGrounded(enemy)) return;
        if (AddJumper(enemy)) return;
    }

    //Funcion por la que aleatoriamente decidimos si un enemigo debe tener escudo o no
    public bool AddShield(EnemyController enemy)
	{
        int perkChecker = Random.Range(0, 100);

        if (perkChecker < CurrentZone.ShieldedChance) //La probabilidad depende de la probabilidad de la zona
        {
            int colorChecker = Random.Range(0, 3);

            if (colorChecker == 0) enemy.ShieldColor = E_COLORS.RED;
            if (colorChecker == 1) enemy.ShieldColor = E_COLORS.BLUE;
            if (colorChecker == 2) enemy.ShieldColor = E_COLORS.YELLOW;

            return true;
        }

        return false;
    }

    //Funcion por la que aleatoriamente decidimos si un enemigo debe ser explosivo
    public bool AddExplosive(EnemyController enemy)
	{
        int alreadyCheckedSum = CurrentZone.ShieldedChance;
        int perkChecker = Random.Range(0, 100 - alreadyCheckedSum); //Reducimos el techo en base a las probabilidades de las mejoras anteriores para no biasear al sistema

        if (perkChecker < CurrentZone.ExplosiveChance) //La probabilidad depende de la probabilidad de la zona
        {
            enemy.ExplosiveEnemy = true;
            return true;
        }
        return false;
    }

    //Funcion por la que aleatoriamente decidimos si un enemigo debe ser disparador
    public bool AddShooter(EnemyController enemy)
    {
        int alreadyCheckedSum = CurrentZone.ShieldedChance + CurrentZone.ExplosiveChance;
        int perkChecker = Random.Range(0, 100 - alreadyCheckedSum); //Reducimos el techo en base a las probabilidades de las mejoras anteriores para no biasear al sistema

        if (perkChecker < CurrentZone.ShooterChance) //La probabilidad depende de la probabilidad de la zona
        {
            enemy.ShooterEnemy = true;
            return true;
        }
        return false;
    }

    //Funcion por la que aleatoriamente decidimos si un enemigo debe ser intangible/estar bajo tierra
    public bool AddGrounded(EnemyController enemy)
    {
        int alreadyCheckedSum = CurrentZone.ShieldedChance + CurrentZone.ExplosiveChance + CurrentZone.ShooterChance;
        int perkChecker = Random.Range(0, 100 - alreadyCheckedSum); //Reducimos el techo en base a las probabilidades de las mejoras anteriores para no biasear al sistema

        if (perkChecker < CurrentZone.GroundedChance) //La probabilidad depende de la probabilidad de la zona
        {
            enemy.Underground = true;
            return true;
        }
        return false;
    }

    //Funcion por la que aleatoriamente decidimos si un enemigo debe ser capaz de saltar entre lineas
    public bool AddJumper(EnemyController enemy)
    {
        int alreadyCheckedSum = CurrentZone.ShieldedChance + CurrentZone.ExplosiveChance + CurrentZone.ShooterChance + CurrentZone.GroundedChance;
        int perkChecker = Random.Range(0, 100 - alreadyCheckedSum); //Reducimos el techo en base a las probabilidades de las mejoras anteriores para no biasear al sistema

        if (perkChecker < CurrentZone.JumperChance) //La probabilidad depende de la probabilidad de la zona
        {
            enemy.JumperEnemy = true;
            return true;
        }
        return false;
    }
}
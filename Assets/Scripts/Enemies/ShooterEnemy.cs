using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el comportamiento especifico de los enemigos que disparan
public class ShooterEnemy : MonoBehaviour
{
    public float TimeBetweenShots = 5f; //Tiempo a tomar entre disparos
    public float TimeSinceLast = 0f;    //Tiempo transcurrido desde el ultimo disparo
    public EnemyController MyEnemy;     //Referencia al enemigo disparador

    public GameObject Bullet;           //prefab de la bala

    // Start is called before the first frame update
    void Start()
    {
        Bullet = MyEnemy.BulletPrefab; //Inicializamos el prefab de la bala
    }

    // Update is called once per frame
    void Update()
    {
        //Si el tiempo desde el ultimo disparo supera el tiempo entre disparos, se dispara
        TimeSinceLast += Time.deltaTime;

        if(TimeSinceLast >= TimeBetweenShots)
		{
            TimeSinceLast = 0f;

            Vector3 pos = new Vector3(transform.position.x -0.3f, transform.position.y, transform.position.z);

            //Para disparar, instanciamos una bala
            BulletController bc = Instantiate(Bullet, pos, Quaternion.identity).GetComponent<BulletController>();
            bc.gameObject.SetActive(true);
            bc.Line = MyEnemy.Line;

        }
    }
}
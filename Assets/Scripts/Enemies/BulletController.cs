using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el funcionamiento de las balas disparadas por los Shooter enemies
public class BulletController : MonoBehaviour
{
    public float BaseSpeed = 3f;                    //Velocidad base a la que se mueve la bala

    public bool Hitted;                             //Booleano que indica si la bala ha golpeado al jugador o no
    public GameplayManager GM;                      //GameplayManager de la escena

    public int Line = 1;                            //Linea en la que viaja la bala


    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameplayManager").GetComponent<GameplayManager>(); //Instanciamos el GameplayManager buscandolo en la escena
        Hitted = false; //Originalmente ela bala no habran golpeado al jugador       
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * BaseSpeed * (1 + GM.TimeTotal / 100));

        //En caso de que la bala alcance la posicion del jugador y no le haya golpeado aun
        if (transform.position.x < GM.Roloc.transform.position.x && !Hitted && GM.Roloc.CurrentLine == Line && transform.position.x > -7.75)
        {
            Hitted = true; //Indicaremos que la bala ya ha golpeado al jugador
            GM.DamageRoloc(); //Se realizara la penalizacion del jugador
            
            Destroy(gameObject);
        }

        if (transform.position.x < -10) Destroy(gameObject); //Si la bala se sale de la pantalla, tambien la destruimos
    }
}

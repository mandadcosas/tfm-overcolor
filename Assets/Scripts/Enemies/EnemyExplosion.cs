using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase que controla el comportamiento especifico de los enemigos explosivos
public class EnemyExplosion : MonoBehaviour
{
    public int Line = 1;                //Linea en la que esta el enemigo
    public float ExplosionTime = 3;     //Tiempo antes de la explosion
    public float ExpendedTime = 3;      //Tiempo restante antes de la explosion

    public GameplayManager GM;          //GameplayManager
    public Text Counter;                //Texto con el contador del tiempo

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Explode", ExplosionTime);
    }

    // Update is called once per frame
    void Update()
    {
        //El tiempo se reduce y se relfeja en el contador
        ExpendedTime -= Time.deltaTime;
        int aux = (int) ExpendedTime + 1;
        Counter.text = aux.ToString();
    }

    //Funcion por la que el enemigo explota
    public void Explode()
	{
        GM.ExplosionFromEnemy(Line); //Realizamos el ataque de explosion aen la linea correspondiente
        Destroy(transform.parent.gameObject); //Destruimos el objeto
	}
}

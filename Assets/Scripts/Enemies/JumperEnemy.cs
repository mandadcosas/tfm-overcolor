using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el comportamiento particular de los enemigos saltarines
public class JumperEnemy : MonoBehaviour
{
    public EnemyController MyEnemy;         //Referencia al enemigo saltarin
    public GameplayManager GM;              //GameplayManager

    public float TimeBetweenJumps = 7f;     //Tiempo entre saltos
    public float TimeSinceLastJump = 0f;    //Tiempo transcurrido desde el ultimo salto

    public bool AlreadyJumped = false;

    // Start is called before the first frame update
    void Start()
    {
        TimeSinceLastJump = Random.Range(0, TimeBetweenJumps); //El primer salto se hara de forma aleatoria para que sea impredecible
    }

    // Update is called once per frame
    void Update()
    {
        //Si el tiempo transcurrido desde el ultimo salto supera el definido, el enemigo salta
        TimeSinceLastJump += Time.deltaTime;

        if (TimeSinceLastJump >= TimeBetweenJumps && !AlreadyJumped)
        {
            TimeSinceLastJump = 0f;

            Jump();

        }
    }

    //Funcion por la que el enemigo salta de una linea a otra
    public void Jump()
	{
        AlreadyJumped = true;

        List<EnemyController> lineToUse = null;
        int r = Random.Range(0, 2);

        GameObject originalFading = Instantiate(MyEnemy.FadeImage, transform.position, Quaternion.identity);
        originalFading.GetComponent<SpriteRenderer>().sprite = MyEnemy.GetComponent<SpriteRenderer>().sprite;

        //Desde la Linea 1, solo se puede saltar a la Linea 2
        if (MyEnemy.Line == 1)
		{
            lineToUse = GM.EnemiesLine1;
            EnemyController reference = lineToUse.Find(x => x.Equals(MyEnemy));
            if (reference == null) return; //Con esto evitamos algunos posibles bugs de enemigos que mueren en momentos muy precisos
            lineToUse.Remove(reference);
            GM.EnemiesLine2.Add(reference);

            if (reference.gameObject != null)
			{
                reference.transform.position = new Vector3(reference.transform.position.x, GM.Line2.transform.position.y, reference.transform.position.z);

                Vector3 FadeImagePos = new Vector3(reference.transform.position.x, (GM.Line1.transform.position.y + GM.Line2.transform.position.y) / 2, reference.transform.position.z);
                GameObject fading = Instantiate(MyEnemy.FadeImage, FadeImagePos, Quaternion.identity);
                fading.GetComponent<SpriteRenderer>().sprite = reference.GetComponent<SpriteRenderer>().sprite;
            }               
        }
        //Desde la Linea 2 se puede saltar tanto a la 1 como a la 3, lo cual se decide aleatoriamente
        else if (MyEnemy.Line == 2)
        {
            lineToUse = GM.EnemiesLine2;
            if (r == 0)
            {
                EnemyController reference = lineToUse.Find(x => x.Equals(MyEnemy));
                if (reference == null) return; //Con esto evitamos algunos posibles bugs de enemigos que mueren en momentos muy precisos
                lineToUse.Remove(reference);
                GM.EnemiesLine1.Add(reference);

                if (reference.gameObject != null)
				{
                    reference.transform.position = new Vector3(reference.transform.position.x, GM.Line1.transform.position.y, reference.transform.position.z);

                    Vector3 FadeImagePos = new Vector3(reference.transform.position.x, (GM.Line1.transform.position.y + GM.Line2.transform.position.y) /2, reference.transform.position.z);
                    GameObject fading =  Instantiate(MyEnemy.FadeImage, FadeImagePos, Quaternion.identity);
                    fading.GetComponent<SpriteRenderer>().sprite = reference.GetComponent<SpriteRenderer>().sprite;
                }                                    
            }
            else
            {
                EnemyController reference = lineToUse.Find(x => x.Equals(MyEnemy));
                if (reference == null) return; //Con esto evitamos algunos posibles bugs de enemigos que mueren en momentos muy precisos
                lineToUse.Remove(reference);
                GM.EnemiesLine3.Add(reference);

                if (reference.gameObject != null)
				{
                    reference.transform.position = new Vector3(reference.transform.position.x, GM.Line3.transform.position.y, reference.transform.position.z);

                    Vector3 FadeImagePos = new Vector3(reference.transform.position.x, (GM.Line2.transform.position.y + GM.Line3.transform.position.y) /2, reference.transform.position.z);
                    GameObject fading =  Instantiate(MyEnemy.FadeImage, FadeImagePos, Quaternion.identity);
                    fading.GetComponent<SpriteRenderer>().sprite = reference.GetComponent<SpriteRenderer>().sprite;
                }                                   
            }
        }
        //Desde la Linea 3, solo se puede saltar a la Linea 2
        else
        {
            lineToUse = GM.EnemiesLine3;
            EnemyController reference = lineToUse.Find(x => x.Equals(MyEnemy));
            if (reference == null) return; //Con esto evitamos algunos posibles bugs de enemigos que mueren en momentos muy precisos
            lineToUse.Remove(reference);
            GM.EnemiesLine2.Add(reference);

            if (reference.gameObject != null)
			{
                reference.transform.position = new Vector3(reference.transform.position.x, GM.Line2.transform.position.y, reference.transform.position.z);

                Vector3 FadeImagePos = new Vector3(reference.transform.position.x, (GM.Line2.transform.position.y + GM.Line3.transform.position.y) /2, reference.transform.position.z);
                GameObject fading =  Instantiate(MyEnemy.FadeImage, FadeImagePos, Quaternion.identity);
                fading.GetComponent<SpriteRenderer>().sprite = reference.GetComponent<SpriteRenderer>().sprite;
            }
                
        }     
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que controla el comportamiento especial de los enemigos bicolor
//Hereda de EnemyController
public class DoubleColoredEnemy : EnemyController
{
    //Sobreescribimos el metodo de morir a manos del jugador haciendo que sean afectados por sus dos colores
    //Al morir, spawnearan un enemigo simple del color que no fue golpeado
	public override bool KillByPlayer(E_COLORS color)
	{
        if (!CheckShield(color)) return false;
        if (Underground) return false;

        //return base.KillByPlayer(color);
        if (enemyColor == color || Hitted)
        {
            ManageDestruction(); //El enemigo sera destruido al cabo de un ligero retardo para evitar bugs de sincronizacion

            //En base al color del enemigo, el jugador recibira divisa de ese color
            if (color == E_COLORS.RED)
            {
                Currencies.RedCurrency += (CurrencyValue * GM.RedMultiplier);               
            }
            else if (color == E_COLORS.BLUE)
            {
                Currencies.BlueCurrency += (CurrencyValue * GM.BlueMultiplier);
            }
            else if (color == E_COLORS.YELLOW)
            {
                Currencies.YellowCurrency += (CurrencyValue * GM.YellowMultiplier);
            }

            SpawnByColor(enemySecondaryColor, Line);
            return true; //Indicamos que el enemigo fue derrotado
        }
        else if (enemySecondaryColor == color)
		{
            ManageDestruction(); //El enemigo sera destruido al cabo de un ligero retardo para evitar bugs de sincronizacion

            //En base al color del enemigo, el jugador recibira divisa de ese color
            if (color == E_COLORS.RED)
            {
                Currencies.RedCurrency += (CurrencyValue * GM.RedMultiplier);
            }
            else if (color == E_COLORS.BLUE)
            {
                Currencies.BlueCurrency += (CurrencyValue * GM.BlueMultiplier);
            }
            else if (color == E_COLORS.YELLOW)
            {
                Currencies.YellowCurrency += (CurrencyValue * GM.YellowMultiplier);
            }

            SpawnByColor(enemyColor, Line);
            return true; //Indicamos que el enemigo fue derrotado
        }
        //En caso contrario
        else
        {
            return false; //Indicamos que el enemigo no fue derrotado
        }
    }

    //Funcion que gestiona el spawn de un enemigo basico de un color concreto en una linea concreta
    //Usado para determinar que enemigo ha de spawnear tras vencer a un enemigo bicolor
    public void SpawnByColor(E_COLORS color, int line)
	{       
        EnemyGenerator EG = GameObject.Find("EnemyGenerator").GetComponent<EnemyGenerator>();

        if (color == E_COLORS.RED)
        {
            EG.SpawnSpecificEnemy(EG.RedEnemy, line, transform.position);
        }
        else if (color == E_COLORS.BLUE)
        {
            EG.SpawnSpecificEnemy(EG.BlueEnemy, line, transform.position);
        }
        else if (color == E_COLORS.YELLOW)
        {
            EG.SpawnSpecificEnemy(EG.YellowEnemy, line, transform.position);
        }
    }

	public override void ManageDestruction()
	{
        Destroy(gameObject, 0.01f);
    }
}
